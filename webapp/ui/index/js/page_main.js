/**
 * 创建者：duyh
 * 方法描述：根据绑定的tab的ID值转向指定的页面
 * 所作用的页面名称：main.html
 * 变更时间：
 * 变更人：
 * 变更内容：
 **/
var tuichu = "再按一次退出程序";
var height = document.documentElement.clientHeight;
var searchTitle = document.getElementById("searchTitle");
var meTitle = document.getElementById("meTitle");
var projectTitle = document.getElementById("projectTitle");
var policyTitle = document.getElementById("policyTitle");

(function($) {
	//预加载首页
	mui.init({
		keyEventBind: {
			backbutton: true //开启back按键监听
		}
	});
	clientWidth = document.documentElement.clientWidth;
	setPxPerRem(clientWidth); // 初始化方法
	mui.plusReady(function() {
		plus.navigator.setStatusBarBackground('#1892E8');
		var self = plus.webview.currentWebview();
		for(var i = 0; i < subpages.length; i++) {
			var temp = {};
			var sub;
			sub = plus.webview.create(subpages[i], subpages[i], subpage_style, subpage_value);
			if(i > 0) {
				sub.hide();
			} else {
				temp[subpages[i]] = "true";
				mui.extend(aniShow, temp);
			}
			self.append(sub);
		}

		var backButtonPress = 0;

		mui.back = function(event) {
			backButtonPress++;
			if(backButtonPress > 1) {
				plus.runtime.quit();
			} else {
				plus.nativeUI.toast(tuichu);
			}
			setTimeout(function() {
				backButtonPress = 0;
			}, 1000);
			return false;
		};
	});
})(mui);

var subpages = ['page_index.html', 'page_typeList.html', '../order/page_orderList.html', '../user/page_user.html'];
var subpage_style = {
	top: '0px',
	bottom: '51px'
};

var subpage_value = {
	clientWidth: clientWidth,
	pageflag: 'page_main.html',
	height: height - 50
}
var aniShow = {};

//当前激活选项
var activeTab = subpages[0];
//获取当前标题栏
//var title = document.getElementById("title");
var header = document.getElementById("header");
var xz_num = 0;
//选项卡点击事件
mui('.mui-bar-tab').on('click', 'a', function(e) {
	var targetTab = this.getAttribute('href');
	if(targetTab == activeTab) {
		return;
	}
	var isLogin = localStorage.getItem("isLogin");

//	if(targetTab == "page_project.html") {
//		if(!isLogin) {
//			deleteClass('mui-active', this);
//			mui.toast("请登录后查看");
//			return;
//		}
//	}

	for(var i = 0; i < subpages.length; i++) {
		if(targetTab == subpages[i] && i + 1 == xz_num) {
			return;
		}
	}
	var tabID = "";
	allDefault();
	//更换标题
	if(targetTab == subpages[1]) {
		xz_num = 2;
		tabID = "policyTab";
		document.getElementById("policyImg").src = "../../../ui/index/images/main04.png";
	} else if(targetTab == subpages[2]) {
		xz_num = 3;
		tabID = "resourcesTab";
		document.getElementById("resourcesImg").src = "../../../ui/index/images/main06.png";
	} else if(targetTab == subpages[3]) {
		xz_num = 4;
		tabID = "settingTab";
		document.getElementById("settingImg").src = "../../../ui/index/images/main08.png";
	} else {
		xz_num = 1;
		tabID = "mainTab";
		document.getElementById("mainImg").src = "../../../ui/index/images/main02.png";
	}
	//显示目标选项卡
	//若为iOS平台或非首次显示，则直接显示
	if(mui.os.ios || aniShow[targetTab]) {
		plus.webview.show(targetTab);
	} else {
		//否则，使用fade-in动画，且保存变量
		var temp = {};
		temp[targetTab] = "true";
		mui.extend(aniShow, temp);
		plus.webview.show(targetTab, "fade-in", 300);
	}

	plus.webview.show(targetTab);
	//	}
	//隐藏当前;
	plus.webview.hide(activeTab);
	//更改当前活跃的选项卡
	activeTab = targetTab;
});
//自定义事件，模拟点击“首页选项卡”
document.addEventListener('gohome', function() {
	clickTab("mainTab")
}, false);

function clickTab(tabName) {
	var defaultTab = document.getElementById(tabName);
	//模拟首页点击
	mui.trigger(defaultTab, 'click');
	//切换选项卡高亮
	var current = document.querySelector(".mui-bar-tab>.mui-tab-item.mui-active");
	if(defaultTab !== current) {
		current.classList.remove('mui-active');
		defaultTab.classList.add('mui-active');
	}
}

function allDefault() {
	document.getElementById("mainImg").src = "../../../ui/index/images/main01.png";
	document.getElementById("policyImg").src = "../../../ui/index/images/main03.png";
	document.getElementById("resourcesImg").src = "../../../ui/index/images/main05.png";
	document.getElementById("settingImg").src = "../../../ui/index/images/main07.png";
}

function showTitle(ele) {
	hiddenEle(searchTitle);
	hiddenEle(meTitle);
	hiddenEle(projectTitle);
	hiddenEle(policyTitle);
	showEle(ele);
}

window.addEventListener('changeTab', function(event) {
	var tabId = event.detail.tabId;
	console.log(tabId);
	var itemid = event.detail.itemId;
	clickTab(tabId)
}, false);

window.addEventListener('exitLogin', function() {
	var ws = plus.webview.currentWebview();
	plus.webview.close(ws);
	plus.navigator.setStatusBarBackground('#1892E8');
});