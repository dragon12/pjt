!function (e, $, mui) {
    "use strict";
	
	var config = {
	};
	// 定义页面
	var p = function(config) {
		var t = this;
		t.config = config;
		t.userInfo = null;
		t.adCookieKey = 'ad_hide_expired';
		t.element = {
			adContainer: $('.ad-container'),
			adContent: $('.ad-container .page-ad'),
			adClose: $('.ad-container .close')
		};
		t.init();
	};
	
	var pt = p.prototype;
	
	// 页面初始化
	pt.init = function () {
		var t = this;
		t.userInit();
		// 监听
		t.listen();
		// 页面加载
		t.pageLoad();
	};
	
	// 用户信息初始化
	pt.userInit = function() {
		var t = this;
		t.userInfo = getLocalUserInfo();
	};
	
	// 事件监听
	pt.listen = function () {
		var t = this;
		t.element.adClose.on('tap', function () {
			// 关闭1天
			localStorage.setItem(t.adCookieKey, (new Date()).getTime() + 86400*1000);
			// 关闭广告
			t.element.adContainer.hide();
		});
	}
	
	// 列表初始化
	pt.pageLoad = function () {
		var t = this;
		
		// 判断是否请求广告
		var ad_hide_expired = localStorage.getItem(t.adCookieKey);
		if(ad_hide_expired && ad_hide_expired > (new Date()).getTime()) return;
		
		// 列表加载
		t.adLoad();
	}
	
	// 广告加载
	pt.adLoad = function () {
		var t = this;
		
		baseApi.post({
			op: 'app_index_ad'
		}, function (result) {
			console.log(JSON.stringify(result));
			
			if(!result || !result.ad_tid) return;
			
			var html = '';
			if(result.pic) {
//				html += '<img src="'+getRequestAddressUrl('imageHost')+result.pic+'">';
			}
			html += '<div class="row">'+result.content+'</div>';
			t.element.adContent.html(html);
			t.element.adContainer.show();
		});
	}

	// 页面初始化
	e.page = new p(config);
	mui.plusReady(function () {
		
		var tuichu = "再按一次退出程序";
		var backButtonPress = 0;

		// 关闭所有无用窗口
		clearAllWindows();
		plus.navigator.setStatusBarBackground('#3E7FC3');
		
		mui.back = function(event) {
			backButtonPress++;
			if(backButtonPress > 1) {
				var main = plus.android.runtimeMainActivity();
				main.moveTaskToBack(false);
				return false;
			} else {
				plus.nativeUI.toast(tuichu);
			}
			setTimeout(function() {
				backButtonPress = 0;
			}, 1000);
			return false;
		};
	});

}(window, $, mui);