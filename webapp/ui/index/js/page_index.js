var backButtonPress = 0;
var tuichu = "再次点击退出程序";
var currentWayBill = "0";
var isUpdate = 0; //默认0 不更新
var contactsDrop = document.getElementById("contactsDrop");
var emptyBill = document.getElementById("emptyBill");
var orderContent = document.getElementById("orderContent");
var current_haul_id = "";
var currentBillNo = "";
var hauls;
var offCanvasWrapper = document.getElementById("offCanvasWrapper");

var pageflag = "";

(function(e) {
	$('.bottom-nav .sub-nav-container').on('tap', function () {
		$(this).find('.sub-nav').toggle();
	});
	
	mui.init({
		keyEventBind: {
			backbutton: true //开启back按键监听
		}
	});
	clientWidth = document.documentElement.clientWidth;

	mui('#content').scroll();

	$("#hiddenCon").click(function() {
		hiddenEle(contactsDrop);
	});

	$("#contacts").on('tap', function(e) {
		showEle(contactsDrop);
	});

	$("#commitBill").on('tap', function(e) {
		var btnArray = ['确定', '取消'];
		mui.confirm('确认完成订单？', '完成订单', btnArray, function(e) {
			if(e.index == 1) {
				//取消
			} else {
				//完成
				waybillsubmit(currentWayBill, "3")
			}
		})
	});

	$("#exCommit").bind("tap", function(e) {
		if(isEmpty(currentWayBill) || currentWayBill == "0" || currentWayBill == "-1") {
			return;
		}

		pageChange("../index/page_ex_commit.html",{
				pageflag: "page_index.html",
				billID: currentWayBill,
				billNo: currentBillNo,
				h_id: current_haul_id
			});

	});

	getUserInfo();

	setInterval(function () {
		waybillCheckTimer();
	}, 8*1000);
})(mui);

$("#refresh").click(function() {
	getWayBillInfo();
});

// 页面用户信息填充
function getUserInfo() {

	//	if(offCanvasWrapper.className.indexOf("mui-active") > -1) {
	//		offCanvasWrapper.offCanvas('close');
	//	}

	baseApi.post({
		op: 'getuserinfo'
	}, function(result) {
		userInfo = result;
		currentWayBill = userInfo.current_waybill;

		setLocalUserInfo(result);
		waybillCheck();
	});
}

// 运单定时检测
function waybillCheckTimer() {
	if(!waybillIsNotEmpty()) {
		// 当前运单为空，服务器请求判断
		getUserInfo();
	}
	// 待接运单检测
	unacceptinfo();
}

function waybillIsNotEmpty() {
	return (!isEmpty(currentWayBill) && currentWayBill != "0" && currentWayBill != "-1");
}

// 当前运单检测
function waybillCheck() {
	if(waybillIsNotEmpty()) {
		getWayBillInfo();
	}
}

// 未确认订单
function unacceptinfo() {
	baseApi.post({
		op: 'unacceptinfo',
		vcode: userInfo.vhcCode
	}, function(result) {
		var unacceptNum = parseInt(result.unaccept);
		var historyNum = result.history;
		if(unacceptNum > 0) {
			showEle(orderPoint);
		}
	});
}

var waybillInfo = {};
$('.info .qrcode').on('tap', function () {
	if(waybillInfo.ImageUrl) {
		openBigImage(waybillInfo.ImageUrl, true);
	}
});

function getWayBillInfo() {

	baseApi.post({
		op: "waybill",
		receive: isUpdate,
		wmtid: currentWayBill
	}, function(result) {
		waybillInfo = result;
		if(waybillInfo.ImageUrl) {
			$('.info .qrcode').show();
		} else {
			$('.info .qrcode').hide();
		}
		
		currentWayBill = result.wm_tid;
		$('.add-box_no').data('wmtid', result.wm_tid);
		$('.box-list').data('wmtid', result.wm_tid);
		$("#wm_type").html(result.wm_type);
		currentBillNo = result.wm_code;
		$("#waybillNo").html(result.wm_code);
		$("#paperless").html(result.wm_paperless);
		$("#receiptCode").html(result.wm_receipt_code);
		$("#reserveCode").html(result.wm_reserve_code);
		$("#billNo").html(result.wm_awb_code);
		$("#shapCompany").html(result.ShipCompany);
		$("#address").html(result.wm_loading_place);
		$("#time").html(result.wm_loading_date);
		$("#coType").html(result.CX);
		//		$("#coNo").html(result.boxes[0]);
		$("#shapName").html(result.wm_ship + "/" + result.wm_voyage);
		$("#AMTN").html(result.AMTN + "元");
		$("#wm_note").html(result.wm_note);
		var contacts = result.contacts;
		current_haul_id = result.wm_haul_id;

		if(!isEmpty(contacts)) {
			removeLi("contactsList");
			for(var i = 0; i < contacts.length; i++) {
				createContactsItem(contacts[i]);
			}
		}
		hauls = result.haul;

		if(!isEmpty(hauls)) {
			removeLi("fortuneList");
			for(var i = 0; i < hauls.length; i++) {
				createFortuneItem(hauls[i]);
			}
		}

		$("#fortuneList li").bind("tap", function() {
			var itemID = this.id;

			var itemStr = "";

			for(var i = 0; i < hauls.length; i++) {
				if(hauls[i].h_tid == itemID) {
					itemStr = JSON.stringify(hauls[i]);
				}
			}
			console.log(itemStr);
			pageChange("../order/page_forture_info.html",{
					pageflag: "page_index.html",
					itemID: itemID,
					itemStr: itemStr
				});
		});

		showEle(orderContent);
		hiddenEle(emptyBill);
	});
}
var orderPoint = document.getElementById("orderPoint");

function waybillsubmit(id, type) {
	baseApi.post({
		tid: id,
		type: type,
		op: "waybillsubmit"
	}, function(result) {
		userInfo.current_waybill = "0";
		setLocalUserInfo(userInfo);
		currentWayBill = "0";
		hiddenEle(orderContent);
		showEle(emptyBill);

		if(typeof result.unaccept === Number) {
			if(result.unaccept > 0) {
				showEle(orderPoint);
				var btnArray = ['确定', '取消'];
				mui.confirm('您有新的待接运单，是否接单', '新运单', btnArray, function(e) {
					if(e.index == 1) {
						//取消
					} else {
						//完成
						pageChange("../order/page_waybill_list.html",{
								pageflag: "page_index.html"
							});
					}
				})
			}
		}
	});
}

function createFortuneItem(row) {
	var createLi = document.createElement("li");
	createLi.id = row.h_tid;
	createLi.className = row.h_wmtid;
	var statusStr = '<img src="../../../ui/index/images/jingxingzhong.png" /><span>进行中</span>';
	var lateStr = "";
	if(row.h_finished == 1 || (current_haul_id == row.h_tid && row.h_finished == 0)) {
		statusStr = '<img src="../../../ui/index/images/jingxingzhong.png" /><span>进行中</span>';
	} else if(row.h_finished == 2) {
		statusStr = '<img src="../../../ui/index/images/wancheng_xiao.png" /><span class="success">已完成</span>';
		if(row.h_required_service_date < row.h_finished_date) {
			lateStr = '<div class="time"><img src="../../../ui/index/images/new_wandian.png" /><span>晚点</span></div>';
		}
	} else if(row.h_finished == 0) {
		statusStr = '<img src="../../../ui/index/images/weikaishi.png" /><span class="wait-start">未开始</span>';
	} else if(row.h_finished == -1) {
		statusStr = '<img src="../../../ui/index/images/yichang.png" /><span>异常</span>';
	} else {
		statusStr = '<img src="../../../ui/index/images/weikaishi.png" /><span class="wait-start">未开始</span>';
	}
	createLi.innerHTML = '<div>' + statusStr + '</div><h4>' + row.h_name + '</h4>' + lateStr +
		'<span>要求时间：' + row.h_required_service_date + '</span><span>地址：' + row.h_address + '</span><span>备注：' + row.h_note + '</span>';
	$("#fortuneList").append(createLi);
}

function createContactsItem(row) {
	var createLi = document.createElement("li");
	createLi.id = row.tel;
	createLi.className = "goto-type";
	createLi.innerHTML = '<span>' + row.type + '</span><span>' + row.name + '</span><span>' + row.tel + '</span><a href="tel:' + row.tel + '"><img src="../../../ui/index/images/phone_default.png"></a>';
	$("#contactsList").prepend(createLi);
}