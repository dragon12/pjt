var maxLength = 3;
var imageSrc = new Array();
var currentID = "";
var billID = "";
var h_id = "";
var billNo = "";
var locationJson = {
	address: "",
	date: "",
	lat: "",
	lng: ""
};
var currentType = "";
	(function(e) {
			billID = getQueryString("billID");
			h_id = getQueryString("h_id");
			billNo = getQueryString("billNo");


			// 百度地图API功能
			var map = new BMap.Map("map");
			var point = new BMap.Point(120.389081, 36.073445);
			map.centerAndZoom(point,15);
			map.enableScrollWheelZoom(); //启用地图滚轮放大缩小

			var geolocation = new BMap.Geolocation();
			geolocation.getCurrentPosition(function(r){
				if(this.getStatus() == BMAP_STATUS_SUCCESS){
					var address = r.address.city + ' ' + r.address.street + r.address.street_number;
					var mk = new BMap.Marker(r.point);
					map.addOverlay(mk);
					map.panTo(r.point);
					locationJson.lat = r.point.lat;
					locationJson.lng = r.point.lng;
					var date = new Date();
					var time = formatDateTimeForDate(date);
					locationJson.date = time;
					locationJson.address = address;
					createSubview(address, time);
				}
				else {
					alert('failed'+this.getStatus());
				}
			},{enableHighAccuracy: true})
			//关于状态码
			//BMAP_STATUS_SUCCESS	检索成功。对应数值“0”。
			//BMAP_STATUS_CITY_LIST	城市列表。对应数值“1”。
			//BMAP_STATUS_UNKNOWN_LOCATION	位置结果未知。对应数值“2”。
			//BMAP_STATUS_UNKNOWN_ROUTE	导航结果未知。对应数值“3”。
			//BMAP_STATUS_INVALID_KEY	非法密钥。对应数值“4”。
			//BMAP_STATUS_INVALID_REQUEST	非法请求。对应数值“5”。
			//BMAP_STATUS_PERMISSION_DENIED	没有权限。对应数值“6”。(自 1.1 新增)
			//BMAP_STATUS_SERVICE_UNAVAILABLE	服务不可用。对应数值“7”。(自 1.1 新增)
			//BMAP_STATUS_TIMEOUT	超时。对应数值“8”。(自 1.1 新增)
	})(mui);

function createSubview(address, time) {
	// 创建加载内容窗口
	$('#time').html(time);
	$('#address').html(address);
}

$("#commitEX").click(function() {
	currentType = $("#type").val().trim();
	if(currentType == "0") {
		mui.toast("请选择异常原因");
		return;
	}
	if(checkParams(locationJson)) {
		mui.toast("定位信息失败！");
		return;
	}
	var parames = {
		"op": "exceptionsubmit",
		"note": "",
		"code": billNo,
		"type": currentType,
		"content": "",
		"location": locationJson,
		"h_id": h_id,
		"w_id": billID
	}
	var imageDiv = document.getElementById("imageDiv");
	var images = imageDiv.getElementsByTagName("img");
	
	if(images.length == 1 && images[0].src.indexOf("add_photo") > -1){
		mui.toast("请添加照片后提交");
		return;
	}
	
	exSubmit(parames, uploadImages, function(result) {
		if(result.status == 1) {
			var row = result.result;
			mui.toast(row);
			mui.back();
		}
	})
});

var uploadImages = [];
$("#addExImage").click(function() {
	var imageDiv = document.getElementById("imageDiv");

	var images = imageDiv.getElementsByTagName("img");
	var imageSize = images.length;

	if(this.src.indexOf("add_photo") > -1) {
//		openBigImage(this.src, true);
		var createImage;
		if(imageSize >= maxLength) {
			createImage = this;
		} else {
			createImage = document.createElement("img");
			createImage.className = "insert-img";
			createImage.style.width = "80px";
			createImage.src = '../../../ui/index/images/add_photo.png';
			insertBefore(createImage, document.getElementById("addExImage"));
		}
		console.log(createImage);
		getImage(createImage, false, function(result, file) {
			imageSrc.push(result);
			uploadImages.push(file);
		});
	}
});

function getExType() {
	baseApi.post({
		"op": "exceptiontype"
	}, function(result) {
		for(var i = 0; i < result.length; i++) {
			$("#type").append('<option value="' + result[i].d_key + '">' + result[i].d_value + '</option>')
		}
	});
}