!function (e, $, mui) {
    "use strict";
	
	var config = {
	};
	// 定义页面
	var p = function(config) {
		var t = this;
		t.config = config;
		t.userInfo = null;
		t.params = {
			type: 0	// 0新建 1预约 2失败 3全部
		};
		t.page = {
			page: 0,
			perpage: 5,
			more: true
		};
		t.element = {
			nav: $('.top-nav > a'),
			more: $('.list-more'),
			listContainer: $('.mui-content .list')
		};
		t.init();
	};
	
	var pt = p.prototype;
	
	// 页面初始化
	pt.init = function () {
		var t = this;
		t.userInit();
		// 监听
		t.listen();
		// 列表初始化
//		t.listInit();
	};
	
	// 用户信息初始化
	pt.userInit = function() {
		var t = this;
		t.userInfo = getLocalUserInfo();
	};
	
	// 事件监听
	pt.listen = function () {
		var t = this;
		t.element.nav.on('tap', function () {
			t.params.type = $(this).data('state');
			t.listInit();
		});
	}
	
	// 列表初始化
	pt.listInit = function () {
		var t = this;
		t.element.listContainer.html('');
		t.page = {
			page: 0,
			perpage: 5,
			more: true
		};

		setTimeout(function () {
			var pullRefresh = mui('#list-container').pullRefresh();
			if(pullRefresh.refresh) pullRefresh.refresh(true);
		}, 1000);
		if(t.params.type == 1) {
			t.element.listContainer.append('<li class="top-notice">预约有效时间为6小时，到期后请重新预约</li>');
		}
		
		// 列表加载
		t.listLoad();
	}
	
	// 列表加载
	pt.listLoad = function () {
		var t = this;
		t.page.page++;

		var pullRefresh = mui('#list-container').pullRefresh();

		if(t.page.more === false) {
			if(pullRefresh.endPulldown) pullRefresh.endPulldown();
			if(pullRefresh.endPullup) pullRefresh.endPullup();
			return;
		}
		
		baseApi.post({
			op: 'order_list',
			t: t.params.type,
			row: (t.page.page - 1) * t.page.perpage,
			size: t.page.perpage
		}, function (result) {
			pullRefresh = mui('#list-container').pullRefresh();
			if(pullRefresh.endPullupToRefresh) pullRefresh.endPullupToRefresh();
			pullRefresh = mui('#list-container').pullRefresh();
			if(pullRefresh.endPulldownToRefresh) pullRefresh.endPulldownToRefresh();

			var i = 0;
			result.forEach(function (item) {
				i++;
				var html = '<li><span class="state">'+item.state+'</span>';
				html += '<h4>提单号：'+item.bill+'</h4>';
				html += '<cite>场站</cite><em>'+item.station+'</em>';
				html += '<cite>船公司</cite><em>'+item.company+'</em>';
				html += '<cite>船名/航次</cite><em>'+item.ship_name+' / '+item.voyage+'</em>';
				html += '<cite>提货时间</cite><em>'+item.th_time+'</em>';
				html += '<cite>箱型/箱量</cite><em>'+item.box+'/'+item.quantity+'</em>';
				html += '</li>';
				t.element.listContainer.append(html);
			});

			if(t.page.page == 1 && result.length == 0) {
				t.element.listContainer.html('<div id="emptyDiv" class="null-list-div" style="height: 617px;"><img src="../../../ui/index/images/no_bill.png"><br><span>暂无运单信息</span></div>');
			}
			pullRefresh = mui('#list-container').pullRefresh();
			if(i < t.page.perpage) {
				t.page.more = false;
				if(pullRefresh.disablePullupToRefresh) pullRefresh.disablePullupToRefresh();
			} else {
				if(pullRefresh.enablePullupToRefresh) pullRefresh.enablePullupToRefresh();
			}
		});
	}
	
	// 页面初始化
	e.page = new p(config);
	mui.init({
		pullRefresh: {
			container: '#list-container',
			down: {
				style: 'circle',
				contentrefresh: '正在加载...',
				callback: function () {
					e.page.listInit();
				}	// 下拉刷新
			},
			up: {
				auto: false,
				contentrefresh: '正在加载...',
				contentdown: '数据加载',
				callback: function () {	// 上拉加载
					e.page.listLoad(this);
				}
			}
		}
	});
	e.page.listInit();
	mui.plusReady(function () {
		mui.init({
			beforeback: function () {
				// 刷新上一页
				plus.webview.currentWebview().opener().evalJS('getUserInfo();');
			}
		});
	});

}(window, $, mui);