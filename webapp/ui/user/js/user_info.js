!function (e, $, mui) {
    "use strict";
	
	var config = {
		sex: {
			0: '',
			1: '男',
			2: '女',
		}
	};
	// 定义页面
	var p = function(config) {
		var t = this;
		t.config = config;
		t.userInfo = null;
		t.element = {
			nickname: $('.nickname'),
			birthday: $('.birthday'),
			sex: $('.sex'),
			mobile: $('.mobile')
		};
		t.init();
	};
	
	var pt = p.prototype;
	
	// 页面初始化
	pt.init = function () {
		var t = this;
		t.userInit();
		// 页面初始化
		t.element.nickname.text(t.userInfo.real_name);
		t.element.birthday.text(t.userInfo.birthday? t.userInfo.birthday: '');
		t.element.sex.text(t.config.sex[t.userInfo.gender]);
		t.element.mobile.text(t.userInfo.phone_tel);
	};
	
	// 用户信息初始化
	pt.userInit = function() {
		var t = this;
		t.userInfo = getLocalUserInfo();
	};

	e.page = new p(config);
	// 页面初始化
	mui.plusReady(function () {
		mui.init({
			beforeback: function () {
				// 刷新上一页
				plus.webview.currentWebview().opener().evalJS('getUserInfo();');
			}
		});
	});

}(window, $, mui);