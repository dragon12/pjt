var oldPasswdInput = mui(".old-passwd")[0];
var passwdInput = mui(".passwd")[0];
var repeatPasswdInput = mui(".repeat-passwd")[0];

$(".submit").on('tap', function() {
	var oldPasswd = oldPasswdInput.value.trim();
	var passwd = passwdInput.value.trim();
	var repeatPasswd = repeatPasswdInput.value.trim();
	
	if(!oldPasswd) {
		mui.toast('请输入原密码');
		return false;
	}
	if(!passwd) {
		mui.toast('请输入新密码');
		return false;
	}
	if(passwd != repeatPasswd) {
		mui.toast('两次输入密码不一致');
		return false;
	}
	
	baseApi.post({
		op: 'password',
		oldpsw: oldPasswd,
		newpsw: passwd
	}, function (result) {
		mui.toast('操作成功');
		if(isPlusReady()) {
			// 关闭当前页
			plus.webview.currentWebview().close();
		} else {
			history.go(-1);
		}
	});
});