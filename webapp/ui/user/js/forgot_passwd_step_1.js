!function (e, $, mui) {
    "use strict";
	var config = {
	};
	// 定义页面
	var p = function(config) {
		var t = this;
		t.config = config;
		t.userInfo = null;
		t.element = {
			mobile: $('.mobile'),
			submit: $('.submit')
		};
		// 初始化
		t.init();
		
		// 事件监听
		t.listen();
	};
	
	var pt = p.prototype;
	
	// 页面初始化
	pt.init = function () {
		var t = this;
	};
	
	// 事件初始化
	pt.listen = function() {
		var t = this;
		
		t.element.submit.on('tap', function () {
			var mobile = t.element.mobile.val().trim();
			if(!mobile) {
				mui.toast('请输入手机号码');
				return ;
			}
			// 订单数		
			baseApi.post({
				op: 'authcode',
				t: mobile,
				type: '找回密码'
			}, function(result) {
				mui.toast('验证码发送成功');
				pageChange("../user/forgot_passwd_step_2.html", {
						mobile: mobile,
						pageflag: '../user/forgot_passwd_step_2.html'
				});
			});
		});
	}

	e.page = new p(config);
}(window, $, mui);