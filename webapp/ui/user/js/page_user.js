!function (e, $, mui) {
    "use strict";
	var config = {
	};
	// 定义页面
	var p = function(config) {
		var t = this;
		t.config = config;
		t.userInfo = null;
		t.element = {
			nickname: $('.nickname'),
			waybillNum: $('.waybill_num'),
			carNo: $('.car_no')
		};
		// 初始化
		t.init();
		
		// 事件监听
		t.listen();
	};
	
	var pt = p.prototype;
	
	// 页面初始化
	pt.init = function () {
		var t = this;
		t.userInit();
		// 页面初始化
		t.element.nickname.text(t.userInfo.real_name);
		t.element.carNo.text(t.userInfo.vhcCode || '');
	
		// 订单数		
		baseApi.post({
			op: 'waybillnum'
		}, function(result) {
			t.element.waybillNum.text(result.num);
		});
		
	};
	
	// 事件初始化
	pt.listen = function() {
		if(inWechat()) {
			$('.exit-btn').hide();  // 微信内隐藏退出按钮
			return;
		}
		$('.exit-btn').on('tap', function () {
			mui('#sheet1').popover('toggle');
		});
		$('#logout').on('tap', function () {
			userLogout();
		});
		$('#exit-app').on('tap', function () {
			plus.runtime.quit();
		});

		$('#system-version').on('tap', function () {
			mui.alert('系统版本：v' + version, '系统提示')
		});
	}
	
	// 用户信息初始化
	pt.userInit = function() {
		var t = this;
		t.userInfo = getLocalUserInfo();
	};

	e.page = new p(config);

	// 页面初始化
	mui.plusReady(function () {
		mui.init({
			beforeback: function () {
				// 刷新上一页
				plus.webview.currentWebview().opener().evalJS('getUserInfo();');
			}
		});
	});

}(window, $, mui);