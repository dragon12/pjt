var currentAuthType = "0";
var submmitAuthType = "0";
var userCarNo = "";
var obversePath = "";
var directPath = "";
var carLicensePath = "";
var carHeaderPath = "";

(function(e) {

	mui.init({
		keyEventBind: {
			backbutton: true //开启back按键监听
		}
	})


	userCarNo = userInfo.vhcCode;
	getAuthinfo(currentAuthType);

	mui.back = function(event) {
		if(isShow(carAddDiv)) {
			hiddenAllAuth();
			showEle(carCertified);
			return;
		} else {
			closeCurrentWindow();
		}
	};

})(mui);

$("#titleTab span").click(function() {
	if(this.className == "active") {
		return;
	}
	$("#titleTab span").attr("class", "");
	$(this).attr("class", "active");
	currentAuthType = this.id;
	getAuthinfo(currentAuthType);
});

var nameCertified = document.getElementById("nameCertified");
var carCertified = document.getElementById("carCertified");
var operatorCer = document.getElementById("operatorCer");
var carAddDiv = document.getElementById("carAddDiv");

var authPics = [];
function getAuthinfo(authType) {
	baseApi.post({
		"op": "authinfo",
		"a_type": authType
	}, function(result) {
		hiddenAllAuth();
		// 初始化认证数据
		authPics = [
			{
				"key": "身份证正面",
				"val": ''
			}, {
				"key": "身份证反面",
				"val": ''
			}
//			, {
//				"key": "驾驶证正面",
//				"val": ''
//			}, {
//				"key": "驾驶证反面",
//				"val": ''
//			}
		];
		if(authType == "0") {
			showEle(nameCertified);
			var row = null;
			if(isEmpty(result) || result.length <= 0) {
				return;
			} else {
				row = result[0];
			}
			var authStatus = "未审核";
			
			if(!row.a_id_card_no) {
				authStatus = "请完善身份信息";
			} else if(row.a_authed == "1") {
				authStatus = "已审核";
			} else if(row.a_authed == "2") {
				authStatus = "退回";
				$("#authStatus").html("审核状态：" + authStatus);
				return;
			}
			$("#authStatus").html("审核状态：" + authStatus);
			if(authStatus != "请完善身份信息") {
				$("#nameAuthBtn").hide();
				$("#a_id_card_no").attr('readonly', true);
			}
			$("#a_username").val(row.a_username);
			$("#a_id_card_no").val(row.a_id_card_no);
			$("#a_username").attr("readonly", true);
			var pics = row.pics;
			if(pics) {
				for(var i = 0; i < pics.length; i++) {
					console.log(getRequestAddressUrl("imageHost") + pics[i].ap_value);
					$('.auth-pics[data-name="'+pics[i].ap_key+'"]').attr("src", getRequestAddressUrl("imageHost") + pics[i].ap_value);
					
					for(var index in authPics) {
						if(authPics[index].key == pics[i].ap_key) {
							authPics[index].val = pics[i].ap_value;
						}
					}
				}
			}
			
		} else if(authType == "1") {
			showEle(carCertified);
			removeLi("carList");
			for(var i = 0; i < result.length; i++) {
				createCarItem(result[i]);
			}
			//usecar上车    offcar下车
			$("#carList button").click(function() {
				var funStr = "usecar";
				var type = this.innerHTML;
				if(type == "下车") {
					funStr = "offcar";
				}
				var code = this.id;
				baseApi.post({
					op: funStr,
					code: code
				}, function(result) {

					if(funStr == "usecar") {
						userInfo.vhcCode = code;
						setLocalUserInfo(userInfo);
					} else {
						userInfo.vhcCode = "";
						setLocalUserInfo(userInfo);
					}
					mui.toast(type + "成功");
					getAuthinfo("1")
				});

			});
		} else if(authType == "2") {
			showEle(operatorCer);
			removeLi("operatorList");
			for(var i = 0; i < result.length; i++) {
				createOperaItem(result[i]);
			}
			$("#operatorList em").click(function() {
				var code = this.id;
				baseApi.post({
					op: 'auth_operator_cancel',
					code: code
				}, function(result) {
					mui.toast("删除成功");
					getAuthinfo("2")
				});

			});
		}
	});
}

$("#addCarAuth").click(function() {
	hiddenAllAuth();
	vehiclebrandmodel();
	showEle(carAddDiv);
});

$("#addCarBtn").click(function() {
	submmitAuthType = "1";

	var a_vhc_code = $("#a_vhc_code").val().trim();
//	var a_vhc_size = $("#a_vhc_size").val().trim();
//	var a_vhc_weight = $("#a_vhc_weight").val().trim();
	var a_vhc_model = $("#a_vhc_model").html().trim();
	if(isEmpty(a_vhc_code)) {
		mui.toast("请输入车牌号");
		return;
	}

//	if(isEmpty(carLicensePath) || isEmpty(carHeaderPath)) {
//		mui.toast("请提交车辆照片");
//		return;
//	}

	if(isEmpty(a_username) || isEmpty(a_vhc_model)) {
		mui.toast("请完善车辆信息后再试");
		return;
	}
	var paraems = {
		"op": "auth",
		"a_vhc_code": a_vhc_code,
//		"a_vhc_size": a_vhc_size,
//		"a_vhc_weight": a_vhc_weight,
		"a_vhc_model": a_vhc_model,
		"a_type": submmitAuthType,
		"pics": [, {
			"key": "行车证",
			"val": carLicensePath
		}]
	}
	auth(paraems, submmitAuthType);
});

$("#operatorAuth").click(function() {
	submmitAuthType = "2";
	var operatorCode = $("#operatorCode").val().trim();
	if(isEmpty(operatorCode)) {
		mui.toast("请输入运营商编码");
		return;
	}
	var paraems = {
		"op": "auth",
		"a_operator_code": operatorCode,
		"a_type": submmitAuthType
	}
	auth(paraems, submmitAuthType);
});

$("#nameAuthBtn").click(function() {
	submmitAuthType = "0";
	var a_username = $("#a_username").val().trim();
	var a_id_card_no = $("#a_id_card_no").val().trim();
	// var a_driver_licence = $("#a_driver_licence").val().trim();

	if(isEmpty(a_username) || isEmpty(a_id_card_no)) {
		mui.toast("请完善个人信息后再试");
		return;
	}
	
	for(var index in authPics) {
		// if(authPics[index].val == '') {
		// 	mui.toast("请上传 "+authPics[index].key);
		// 	return;
		// }
	}
	
	var paraems = {
		"op": "auth",
		"a_username": a_username,
		"a_id_card_no": a_id_card_no,
		"a_type": submmitAuthType,
		"pics": authPics
	}
	auth(paraems, submmitAuthType);
});

$(".auth-pics").click(function() {
	var indexName = $(this).data('name');
	if(this.src.indexOf("add_photo") > -1) {
		getPhoto(this, true, function(result) {
			if(result.status == 1) {
				var row = result.result;
				if(result.status == "0"){
					mui.toast(row);
				} else {
					for(var index in authPics) {
						if(indexName == authPics[index].key) {
							authPics[index].val = row.storename;
						}
					}
				}
				console.log(JSON.stringify(authPics));
			}
		});
	} else {
		openBigImage(this.src, true);
	}
});

$("#carLicense").click(function() {
	if(this.src.indexOf("add_photo") > -1) {

		getPhoto(this, true, function(result) {
			if(result.status == 1) {
				var row = result.result;
				carLicensePath = row.storename;
			}
		});
	} else {
		openBigImage(this.src, true);
	}
});

$("#carHeader").click(function() {
	if(this.src.indexOf("add_photo") > -1) {
		getPhoto(this, true, function(result) {
			if(result.status == 1) {
				var row = result.result;
				carHeaderPath = row.storename;
			}
		});
	} else {
		openBigImage(this.src, true);
	}
});

function getStrByA_type(a_type) {
	var authStatus = "未审核";
	if(a_type == "1") {
		authStatus = "已审核";
	} else if(a_type == "2") {
		authStatus = "退回";
		$("#authStatus").html("审核状态：" + authStatus);
	}
	return authStatus;
}

function hiddenAllAuth() {
	hiddenEle(nameCertified);
	hiddenEle(carCertified);
	hiddenEle(operatorCer);
	hiddenEle(carAddDiv);
}

function createCarItem(row) {
	var createLi = document.createElement("li");
	createLi.id = row.a_tid;
	var carStatusStr = "上车";
	userCarNo = userInfo.vhcCode;
	if(userCarNo == row.a_vhc_code) {
		carStatusStr = "下车";
	}

	var status = getStrByA_type(row.a_authed);

	createLi.innerHTML = '<span>' + row.a_vhc_code + '</span><div><button id="' +
		row.a_vhc_code + '" class="mui-button-row mui-btn-green">' + carStatusStr + '</button>' +
		'</div><em>' + status + '</em>';
	$("#carList").append(createLi);
}

function createOperaItem(row) {
	var createLi = document.createElement("li");
	createLi.id = row.a_tid;
	var statusCode = row.a_authed;
	var status = "审核中";
	var authTypsStr = '<div><img src="../../../ui/index/images/shenhezhong.png" />';

	if(statusCode == "1") {
		status = "已加盟";
		authTypsStr = '<div class="active"><img src="../../../ui/index/images/yijiameng.png" />';
	}
	createLi.innerHTML = authTypsStr + '<br />' + status + '</div>' +
		'<cite>' + row.o_name + '<br />' + row.a_operator_code + '</cite><em id="' + row.a_operator_code + '">删除</em>';
	$("#operatorList").append(createLi);
}

function auth(parames, authType) {

	if(authType == "0") {

	} else if(authType == "1") {

	}

	baseApi.post(parames, function(result) {
		mui.toast(result);
		if(authType == "0") {
			$("#nameAuthBtn").hide();
			$("#a_username").attr("readonly", true);
			$("#a_driver_licence").attr("readonly", true);
		} else if(authType == "1") {
			hiddenAllAuth();
			showEle(carCertified);
		} else if(authType == "2") {
			$("#operatorCode").val("")
		}
		getAuthinfo(authType);
	});
}
var config = {};
var config_type = "carType";
function vehiclebrandmodel() {
	baseApi.post({
		"op": "vehiclebrandmodel"
	}, function(result) {
		config[config_type] = [];
		result.forEach(function(item) {
			config[config_type].push({
				text: item.d_key,
				value: item.d_value
			});
		});
		picker.setData(config[config_type]);
	});
}
var picker = new mui.PopPicker();



$("#carType").click(function() {
	picker.show(function(item) {
		item = item[0];
		$("#a_vhc_model").html(item.text);
	});
});