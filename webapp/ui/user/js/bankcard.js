!function (e, $, mui) {
    "use strict";
	
	var config = {
	};
	// 定义页面
	var p = function(config) {
		var t = this;
		t.config = config;
		t.userInfo = null;
		t.page = {
			page: 0,
			perpage: 5,
			more: true
		};
		t.element = {
			listContainer: $('.mui-content > .list')
		};
		t.init();
	};
	
	var pt = p.prototype;
	
	// 页面初始化
	pt.init = function () {
		var t = this;
		t.userInit();
		// 监听
		t.listen();
		// 列表初始化
//		t.listInit();
	};
	
	// 用户信息初始化
	pt.userInit = function() {
		var t = this;
		t.userInfo = getLocalUserInfo();
	};
	
	// 事件监听
	pt.listen = function () {
		var t = this;
		
		// 删除
		t.element.listContainer.on('tap', '.del', function() {
			var id = $(this).data('id');
			
			mui.confirm('是否确定删除该条数据，删除后不可恢复？', '删除提示', ['确定', '取消'], function(result) {
				if(result.index == 0) {
					baseApi.post({
						op: 'bankcard_del',
						id: id
					}, function (result) {
						mui.toast('操作成功');
						t.listInit();
					});
				}
			});
		});
	}
	
	// 列表初始化
	pt.listInit = function () {
		var t = this;
		t.element.listContainer.html('');
		
		// 列表加载
		t.listLoad();
	}
	
	// 列表加载
	pt.listLoad = function () {
		var t = this;
		t.page.page++;
		
		if(t.page.more === false) {
			mui('#list-container').pullRefresh().endPulldown();
			mui('#list-container').pullRefresh().endPullup();
			return;
		}
		
		baseApi.post({
			op: 'bankcard',
		}, function (result) {
			var i = 0;
			result.forEach(function (item) {
				i++;
				var html = '<li><em class="fa fa-trash-o del" data-id="'+item.id+'"></em>';
				html += '<cite>银行：</cite><em>'+item.bank+'</em>';
				html += '<cite>开户名：</cite><em>'+item.card_name+'</em>';
				html += '<cite>卡号：</cite><em>'+item.card_no+'</em>';
				html += '</li>';
				t.element.listContainer.append(html);
			});
			if(i == 0) {
				mui.toast('暂无银行卡记录');
			}
			mui('#list-container').pullRefresh().endPulldown();
		});
	}
	
	// 页面初始化
	e.page = new p(config);
	mui.init({
		pullRefresh: {
			container: '#list-container',
			down: {
				style: 'circle',
				callback: function () {
					e.page.listInit();
				}	// 下拉刷新
			}
		}
	});
	e.page.listInit();
	mui.plusReady(function () {
		mui.init({
			beforeback: function () {
			}
		});
	});

}(window, $, mui);