!function (e, $, mui) {
    "use strict";
	var config = {
	};
	// 定义页面
	var p = function(config) {
		var t = this;
		t.mobile = getQueryString('mobile');
		t.code = getQueryString('code');
		t.config = config;
		t.userInfo = null;
		t.element = {
			passwd: $('.passwd'),
			repeat_passwd: $('.repeat-passwd'),
			submit: $('.submit')
		};
		// 初始化
		t.init();
		
		// 事件监听
		t.listen();
	};
	
	var pt = p.prototype;
	
	// 页面初始化
	pt.init = function () {
		var t = this;
	};
	
	// 事件初始化
	pt.listen = function() {
		var t = this;
		
		t.element.submit.on('tap', function () {
			var passwd = t.element.passwd.val().trim();
			var repeatPasswd = t.element.repeat_passwd.val().trim();
			if(!passwd) {
				mui.toast('请输入新密码');
				return ;
			}
			if(passwd != repeatPasswd) {
				mui.toast('两次输入密码不一致');
				return ;
			}
			// 提交验证		
			baseApi.post({
				op: 'getpassword',
				tel: t.mobile,
				newpsw: passwd,
				type: '找回密码',
				code: t.code
			}, function(result) {
				mui.toast('密码重置成功');
				setTimeout(function () {
					if(isPlusReady()) {
						// 返回登录页
						var c = plus.webview.currentWebview();
						var b = c.opener();
						var a = b.opener();
						a.close();
						b.close();
						c.close();
					} else {
						pageChange('../user/login.html');
					}
				}, 1000);
			});
		});
	}
	
	// 页面初始化
	e.page = new p(config);

}(window, $, mui);