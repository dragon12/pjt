var mobileInput = mui(".mobile")[0];
var captchaInput = mui(".captcha")[0];
var passwdInput = mui(".passwd")[0];
var repeatPasswdInput = mui(".repeat-passwd")[0];

(function(mui) {
	var t = 60;
	var getCaptchaEnable = true;
	$('.captcha-get').on('tap', function () {
		$(this).removeClass('text-green').addClass("text-gray");
		if(getCaptchaEnable) {
			var mobile = mobileInput.value.trim();
			if(/^1\d{10}$/.test(mobile) === false) {
				mui.toast('手机号码格式错误');
				return;
			}
			if($('.terms').prop('checked') === false) {
				mui.toast('请阅读并同意用户协议');
				retrun;
			}
			baseApi.post({
				op: 'authcode',
				t: mobile,
				type: '注册'
			}, function (result) {
				captchaTimer();
			});
		}
	});
	
	function captchaTimer() {
		if(t > 0) {
			$('.captcha-get').html(t);
			t--;
			setTimeout(function () {
				captchaTimer();
			}, 1000);
		} else {
			t = 60;
			$('.captcha-get').removeClass('text-gray').addClass("text-green").html('获取验证码');
		}
	}

})(mui);

$(".submit").on('tap', function() {
	var mobile = mobileInput.value.trim();
	var captcha = captchaInput.value.trim();
	var passwd = passwdInput.value.trim();
	var repeatPasswd = repeatPasswdInput.value.trim();
	
	if(!mobile) {
		mui.toast('请输入手机号');
		return false;
	}
	if(!captcha) {
		mui.toast('请输入验证码');
		return false;
	}
	if(!passwd) {
		mui.toast('请输入密码');
		return false;
	}
	if(passwd != repeatPasswd) {
		mui.toast('两次输入密码不一致');
		return false;
	}
	
	baseApi.post({
		op: 'reg',
		t: mobile,
		c: captcha,
		p: passwd,
		wx_token: localCache('wx_token')
	}, function (result) {
		setLocalUserInfo(result);
		pageChange("../index/page_index.html", {
				referer: location.href
		});
	});
});