!function (e, $, mui) {
    "use strict";
	var config = {
	};
	// 定义页面
	var p = function(config) {
		var t = this;
		t.mobile = getQueryString('mobile');
		t.config = config;
		t.userInfo = null;
		t.element = {
			code: $('.mobile-code'),
			timer: $('.resend-timer'),
			submit: $('.submit')
		};
		// 初始化
		t.init();
		
		t.t = 60;
		
		// 事件监听
		t.listen();
	};
	
	var pt = p.prototype;
	
	// 页面初始化
	pt.init = function () {
		var t = this;
	};
	
	// 倒计时
	pt.timer = function () {
		var t = this;
		if(t.t > 0) {
			t.t--;
			t.element.timer.html(t.t + '\'');
			setTimeout(function () {
				t.timer();
			}, 1000);
		} else {
			t.element.timer.html('重新发送');
		}
	},
	
	// 发送验证码
	pt.sendCode = function () {
		var t = this;
		
		// 订单数		
		baseApi.post({
			op: 'authcode',
			t: t.mobile,
			type: '找回密码'
		}, function(result) {
			t.t = 60;
			t.timer();
			mui.toast('验证码发送成功');
		});
	},
	
	// 事件初始化
	pt.listen = function() {
		var t = this;
		
		// 重新发送验证码
		t.element.timer.on('tap', function () {
			if(t.t != 0) return;
			t.sendCode();
		});
		// 倒计时（step 1 已发送验证码）
		t.timer();
		
		t.element.submit.on('tap', function () {
			var code = t.element.code.val().trim();
			if(!code) {
				mui.toast('请输入验证码');
				return ;
			}
			// 提交验证		
			baseApi.post({
				op: 'valicode',
				t: t.mobile,
				type: '找回密码',
				c: code
			}, function(result) {
				pageChange("../user/forgot_passwd_step_3.html", {
						mobile: t.mobile,
						code: code,
						pageflag: '../user/forgot_passwd_step_3.html'
				});
			});
		});
	}
	
	// 页面初始化
	e.page = new p(config);

}(window, $, mui);