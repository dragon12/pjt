(function (mui, $) {
	var sexConfig = [
		{text: '请选择', value: 0},
		{text: '男', value: 1},
		{text: '女', value: 2}
	];
	
	var nicknameInput = $('.nickname');
	var sexInput = $('.sex');
	var birthdayInput = $('.birthday');
	var mobileInput = $('.mobile');
	
	nicknameInput.val(userInfo.real_name);
	sexInput.text(sexConfig[userInfo.gender].text);
	birthdayInput.text(userInfo.birthday || '');
	mobileInput.val(userInfo.phone_tel);
	
    var dtPicker = new mui.DtPicker({
	    	type:'date',
	    	beginYear: (new Date()).getYear() - 80
    	});
    var picker = new mui.PopPicker();
    
	var formdata = {
		op: 'edituser',
		user_id: userInfo.user_id,
		real_name: userInfo.real_name,
		gender: userInfo.gender,
		birthday: userInfo.birthday,
		phone_tel: userInfo.phone_tel
	};
    
    // 性别选择
    sexInput.parent().on('tap', function () {
    	picker.setData(sexConfig);
	    picker.show(function (item) {
	    	item = item[0];
	    	sexInput.text(item.text);
	    	formdata.gender = item.value;
	    });
    });
    
    // 生日
    birthdayInput.parent().on('tap', function () {
	    dtPicker.show(function (item) {
	    	birthdayInput.text(item.text);
	    	formdata.birthday = item.text;
	    });
    });
    
    mui.plusReady(function () {
    	plus.webview.all().forEach(function (item) {
    		console.log(item.id);
    	});
    });
	// 提交
	$('.form-submit').on('tap', function() {
		formdata.real_name = nicknameInput.val().trim();
		formdata.phone_tel = mobileInput.val().trim();

		baseApi.post(formdata, function (result) {
			userInfoUpdate(result);
			if(isPlusReady()) {
				// 更新前一页
				plus.webview.currentWebview().opener().evalJS("window.page.init();");
				// 关闭当前页
				plus.webview.currentWebview().close();
			} else {
				history.go(-1);
			}
		});
		return false;
	});
    
}(mui, $));
