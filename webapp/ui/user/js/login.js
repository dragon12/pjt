var loginName = document.getElementById("userName");
var loginPwd = document.getElementById("passWord");
var access_token = "";
var tuichu = "再按一次退出程序";
var backButtonPress = 0;
var imageDiv = document.getElementById("imageDiv");
var content = document.getElementById("content");

(function(e) {

	mui.init({
		keyEventBind: {
			backbutton: true //开启back按键监听
		}
	});

	mui.plusReady(function() {
		// 关闭所有无用窗口
		clearAllWindows();
		plus.navigator.setStatusBarBackground('#3E7FC3');

		var userInfo = getLocalUserInfo();

		if(isEmpty(userInfo)) {
			plus.navigator.setStatusBarBackground('#31BA25');
			hiddenEle(imageDiv);
			showEle(content);
		} else {
			pageChange("../index/index.html",{
					pageflag: '../user/login.html'
			});
		}

		mui.back = function(event) {
			backButtonPress++;
			if(backButtonPress > 1) {
				plus.runtime.quit();
			} else {
				plus.nativeUI.toast(tuichu);
			}
			setTimeout(function() {
				backButtonPress = 0;
			}, 1000);
			return false;
		};

	});

})(mui);

$("#loginBtn").click(function() {
	getToken();
});

//登录
function getToken() {
	var username = loginName.value.trim();
	var passWord = loginPwd.value.trim();
	
	if(!username) {
		mui.toast('请输入用户名');
		return ;
	}
	if(!passWord) {
		mui.toast('请输入密码');
		return ;
	}

	baseApi.post({
		op: 'login',
		a: username,
		p: passWord,
		wx_token: localCache('wx_token')
	}, function(result) {
		userInfo = result;
		setLocalUserInfo(result);
		pageChange("../index/index.html",{
				pageflag: '../user/login.html'
		});
	});
}