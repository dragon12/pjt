!function (e, $, mui) {
	"use strict";

	var config = {
	};
	// 定义页面
	var p = function(config) {
		var t = this;
		t.config = config;
		t.userInfo = null;
		t.params = {
			type: 0	// 0新建 1预约 2失败 3全部
		};
		t.page = {
			page: 0,
			perpage: 5,
			more: true
		};
		t.element = {
			nav: $('.top-nav > a'),
			more: $('.list-more'),
			listContainer: $('.mui-content .list'),
			listLoading: $('.loading-block')
		};
		t.init();
	};

	var pt = p.prototype;

	// 页面初始化
	pt.init = function () {
		var t = this;
		t.userInit();
		// 监听
		t.listen();
		// 列表初始化
//		t.listInit();
	};

	// 用户信息初始化
	pt.userInit = function() {
		var t = this;
		t.userInfo = getLocalUserInfo();
	};

	// 事件监听
	pt.listen = function () {
		var t = this;
		t.element.nav.on('tap', function () {
			t.params.type = $(this).data('state');
			t.listInit();
		});

		$("#titleTab span").click(function() {
			if(this.className == "active") {
				return;
			}
			$("#titleTab span").attr("class", "");
			$(this).attr("class", "active");
			t.listInit();
		});

		$(document).on('tap', "#waybillList li", function() {
			var id = $(this).data('id');
			var accept = $('#titleTab .active').attr('id');
			pageChange("../order/page_order_info.html", {
				orderID: id,
				showType: accept,
				pageflag: "page_index.html"
			});
		});
	}

	// 列表初始化
	pt.listInit = function () {
		var t = this;
		t.element.listContainer.html('');
		t.page = {
			page: 0,
			perpage: 5,
			more: true
		};
		
		if(t.params.type == 1) {
			t.element.listContainer.append('<li class="top-notice">预约有效时间为6小时，到期后请重新预约</li>');
		}

		// 列表加载
		t.listLoad();
	}

	// 列表加载
	pt.listLoad = function () {
		var t = this;
		
		if(t.page.loading) return;
		
		t.page.page++;

		if(t.page.more === false) {
			return;
		}

		let accept = $('#titleTab .active').attr('id');

		t.page.loading = true;
		t.element.listLoading.removeClass('mui-hidden');
		baseApi.post({
			op: "waybilllist",
			accept: accept,
			row: (t.page.page - 1) * t.page.perpage,
			size: t.page.perpage
		}, function (result) {
			
			t.page.loading = false;

			var i = 0;
			result.forEach(function (row) {
				i++;
				var html ='<li data-id="'+row.wm_tid+'"><h4>运单号：' + row.wm_code + '</h4><cite>位置</cite><em>' + row.wm_loading_place + '</em>' +
					'<cite>要求时间</cite><em>' + row.wm_required_service_date + '</em><cite>船公司</cite><em>' + row.ShipCompany + '</em>' +
					'<cite>提单号</cite><em>' + row.wm_awb_code + '</em><cite>箱型箱量</cite><em>' + row.CX + '</em>' +
					'<cite>集装箱号</cite><em>' + row.box_no + '</em><cite>运费</cite><em class="red">' + row.AMTN + '元</em>' +
					'<cite>备注</cite><em>' + row.wm_note + '</em><img class="position-bottom" src="../../../ui/index/images/jinkoutu.png" /></li>';
				t.element.listContainer.append(html);
			});

			if(t.page.page == 1 && result.length == 0) {
				t.element.listLoading.addClass('mui-hidden');
				t.element.listContainer.html('<div id="emptyDiv" class="null-list-div" style="height: 617px;"><img src="../../../ui/index/images/no_bill.png"><br><span>暂无运单信息</span></div>');
			}


			if(i < t.page.perpage) {
				t.page.more = false;
				t.element.listLoading.find('.loading-text').html('已经没有更多了');
			} else {
			}
		});
	}

	// 页面初始化
	e.page = new p(config);
	mui.init({});
	e.page.listInit();
	
	$(window).scroll(function() {
	    if ($(window).scrollTop() + $(window).height() > $(document).height() - 1) {
			e.page.listLoad();
	    }
	});
	mui.plusReady(function () {
		mui.init({
			beforeback: function () {
				// 刷新上一页
				plus.webview.currentWebview().opener().evalJS('getUserInfo();');
			}
		});
	});

}(window, $, mui);






//
//
// var ACCEPT_TYPE = "1";
//
// //下拉刷新
// var isRefresh = false;
//
// function pulldownRefresh() {
// 	removeLi("waybillList");
//
// 	setTimeout(function () {
// 		var pullRefresh = mui('#content').pullRefresh();
// 		if(pullRefresh.refresh) pullRefresh.refresh(true);
// 	}, 400);
// 	isRefresh = true;
// 	tabPageNum = 0;
// 	waybilllist(ACCEPT_TYPE);
// }
//
// //上拉加载
// var isLoad = false;
//
// function pullupRefresh() {
// 	isLoad = true;
// 	waybilllist(ACCEPT_TYPE);
// }
//
//
//
//
//
// var tabPageNum = 0;
// var pageSize = 10;
//
// function waybilllist(accept) {
// 	let pullRefresh = mui('#content').pullRefresh();
//
// 	if(pullRefresh.endPulldown) pullRefresh.endPulldown();
// 	if(pullRefresh.endPullup) pullRefresh.endPullup();
// 	baseApi.post({
// 		op: "waybilllist",
// 		accept: accept,
// 		row: tabPageNum * pageSize,
// 		size: pageSize
// 	}, function(result) {
//
// 		if(isRefresh) {
// 			isRefresh = false;
// 		}
// 		if(pullRefresh.endPullupToRefresh) pullRefresh.endPullupToRefresh();
// 		if(pullRefresh.endPulldownToRefresh) pullRefresh.endPulldownToRefresh();
//
// 		var size = parseInt(result.length);
// 		if(size > 0) {
// 			hideEmptyDiv();
// 			tabPageNum += size;
// 			for(var i = 0; i < size; i++) {
// 				createItem(result[i]);
// 			}
// 			if(size < pageSize) {
// 				isLoad = false;
// 			}
// 		} else if(tabPageNum != 0 && size == 0) {
// 			isLoad = false;
// 		} else {
// 			var heaght = $("body").height() - $("#header").height();
// 			emptyShow("暂无运单信息", heaght, "header");
// 		}
//
// 		if(isLoad) {
// 			isLoad = false;
// 		}
//
// 	} , function(){
// 		if(pullRefresh.endPullupToRefresh) pullRefresh.endPullupToRefresh();
// 		if(pullRefresh.endPulldownToRefresh) pullRefresh.endPulldownToRefresh();
// 	});
// }
//
// function createItem(row) {
// 	var createLi = document.createElement("li");
// 	createLi.id = row.wm_tid;
// 	createLi.innerHTML = '<h4>运单号：' + row.wm_code + '</h4><cite>位置</cite><em>' + row.wm_loading_place + '</em>' +
// 		'<cite>要求时间</cite><em>' + row.wm_required_service_date + '</em><cite>船公司</cite><em>' + row.ShipCompany + '</em>' +
// 		'<cite>提单号</cite><em>' + row.wm_awb_code + '</em><cite>箱型箱量</cite><em>' + row.CX + '</em>' +
// 		'<cite>集装箱号</cite><em>BOXNUMBER001</em><cite>运费</cite><em class="red">' + row.AMTN + '元</em>' +
// 		'<cite>备注</cite><em>' + row.wm_note + '</em><img class="position-bottom" src="../../../ui/index/images/jinkoutu.png" />';
// 	$("#waybillList").append(createLi);
// }
//
// $("#titleTab span").click(function() {
// 	let pullRefresh = mui('#content').pullRefresh();
// 	if(this.className == "active") {
// 		return;
// 	}
// 	tabPageNum = 0;
// 	$("#titleTab span").attr("class", "");
// 	$(this).attr("class", "active");
// 	ACCEPT_TYPE = this.id;
// 	if(pullRefresh.enablePullupToRefresh) pullRefresh.enablePullupToRefresh();
// 	pulldownRefresh();
// });
// (function(e) {
//
// 	// mui.init({
// 	// 	pullRefresh: {
// 	// 		container: '#content',
// 	// 		down: {
// 	// 			style: 'circle',
// 	// 			contentrefresh: '正在加载...',
// 	// 			callback: function () {
// 	// 				console.log(111);
// 	// 				pulldownRefresh();
// 	// 			}
// 	// 		},
// 	// 		up: {
// 	// 			auto: false,
// 	// 			contentrefresh: '正在加载...',
// 	// 			callback: pullupRefresh
// 	// 		}
// 	// 	}
// 	// });
// 	// ACCEPT_TYPE = $("#titleTab .active")[0].id;
// 	// waybilllist(ACCEPT_TYPE);
// 	//
// 	// mui.plusReady(function() {
// 	// });
//
// })(mui);
