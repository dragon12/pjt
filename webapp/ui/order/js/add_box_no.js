!function (e, $, mui) {
    "use strict";
	
	var config = {
	};
	
	// 定义页面
	var p = function(config) {
		var t = this;
		t.config = config;
		t.userInfo = null;
		t.params = config.params;
		
		t.formdata = {
			wb_id: getQueryString('wb_id') || 0,
			pic: '',
			box: '',
			boxno: '',
			sealno: '',
			wmtid: t.params.wmtid
		};
		t.element = {
			add_photo: $('.add-photo'),
			photo_preview: $('.photo-preview'),
			box: $('.box'),
			boxno: $('.box_no'),
			sealno: $('.seal_no'),
			submit: $('.mui-submit')
		};
		t.init();
	};
	
	var pt = p.prototype;
	    
    // 配置信息获取
    pt.configReady = function(config_type, callback) {
    	var t = this;
    	if(config[config_type]) {
    		return callback(config[config_type]);
    	} else {
    		switch(config_type) {
    			case 'box':
    				baseApi.post({
						op: 'boxn',
						wmtid: t.params.wmtid
					}, function (result) {
						config[config_type] = [];
						result.forEach(function (item) {
							config[config_type].push({
								text: item.g_box,
								value: item.g_box
							});
						});
						return callback(config[config_type]);
					});
    			break;
    		}
    	}
    }
	
	// 页面初始化
	pt.init = function () {
		var t = this;
		t.userInit();
		// 监听
		t.listen();
		
		if(t.formdata.wb_id) {
			baseApi.post({
				op: 'getboxinfo',
				wb_id: t.formdata.wb_id
			}, function (result) {
				t.formdata = result;
				t.element.box.val(result.box);
				t.element.boxno.val(result.boxno);
				t.element.sealno.val(result.box);
				t.formdata.pic.split(',').forEach(function(item) {
				var img = $('<img class="m-r-10" src="'+item+'">');
				t.element.add_photo.before(img);
				});
			});
		}
	};
	
	// 用户信息初始化
	pt.userInit = function() {
		var t = this;
		t.userInfo = getLocalUserInfo();
	};
	
	// 事件监听
	pt.listen = function () {
		var t = this;
		
		// 选择箱型
		t.element.box.parent().on('tap', function () {
    		var picker = new mui.PopPicker();
			t.configReady('box', function (c) {
				picker.setData(c);
			    picker.show(function (item) {
			    	item = item[0];
			    	t.element.box.text(item.text);
			    	t.formdata.box = item.text;
			    });
			});
		});
		
		// 拍照
		t.element.add_photo.on('tap', function (item) {
			var img = $('<img class="m-r-10" src="../../../ui/index/images/add_photo.png">');
			t.element.add_photo.before(img);
			if(img[0].src.indexOf("add_photo_selected") > -1) {
				openBigImage(img[0], true);
			} else {
				getPhoto(img[0], true, function(result) {
					console.log(result);
					if(result.status == 1) {
						var pics = t.formdata.pic ? t.formdata.pic.split(',') : [];
						pics.push(result.result.storename);
						t.formdata.pic = pics.join(',');
					}
				});
			}
		});
		
		// 提交
		t.element.submit.on('tap', function () {
			var formdata = t.formdata;
			formdata.boxno = t.element.boxno.val().trim();
			formdata.sealno = t.element.sealno.val().trim();
			if(!formdata.box) {
				mui.toast('请选择箱型');
				return;
			}
			if(!formdata.boxno) {
				mui.toast('请输入箱号');
				return;
			}
			if(!formdata.sealno) {
				mui.toast('请输入封号');
				return;
			}
			if(!formdata.pic) {
				mui.toast('请上传照片');
				return;
			}
			// 验证是否可预约
			formdata.op = 'addbox';
	    	baseApi.post(formdata, function (result) {
	    		mui.toast('提交成功');
	    		// 刷新上一页
			    if(isPlusReady()) {
				    plus.webview.currentWebview().opener().evalJS('getWayBillInfo();');
				    plus.webview.currentWebview().close();
			    } else {
				    history.go(-1);
			    }
			});
		});
	}

	// 页面初始化
	e.page = new p({
		params: {wmtid: getQueryString('wmtid')} //获得参数
	});
	
	mui.plusReady(function () {
		mui.init({
			beforeback: function () {
				// 刷新上一页
				plus.webview.currentWebview().opener().evalJS('getWayBillInfo();');
			}
		});
	});

}(window, $, mui);