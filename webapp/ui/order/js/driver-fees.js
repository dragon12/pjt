! function(e, $, mui) {
	"use strict";

	var config = {};
	// 定义页面
	var p = function(config) {
		var t = this;
		t.config = config;
		t.userInfo = null;
		t.params = config.params;
		t.page = {
			page: 0,
			perpage: 5,
			more: true
		};
		t.element = {
			listContainer: $('.mui-content > .list')
		};
		t.init();
	};

	var pt = p.prototype;

	// 页面初始化
	pt.init = function() {
		var t = this;
		t.userInit();
		// 监听
		t.listen();
		// 列表初始化
		//		t.listInit();
	};

	// 用户信息初始化
	pt.userInit = function() {
		var t = this;
		t.userInfo = getLocalUserInfo();
	};

	// 事件监听
	pt.listen = function() {
		var t = this;

		// 删除
		t.element.listContainer.on('tap', '.del', function() {
			var id = $(this).data('id');
			var status = $(this).parent().children("em").eq(4).html().trim();
			if (status == '未审核') {
				mui.confirm('是否确定删除该条数据，删除后不可恢复？', '删除提示', ['确定', '取消'], function(result) {
					if (result.index == 0) {
						baseApi.post({
							op: 'tms',
							method: 'DeleteCarFee',
							params: {
								"tid": id
							}
						}, function(result) {
							if (result.status == 1) {
								mui.toast('操作成功');
								t.listInit();
							} else {
								mui.toast(result.result);
							}

						});
					}
				});
			} else {
				mui.toast('不是未审核状态，不允许删除');
			}
		});
		// 拍照
		t.element.listContainer.on('tap', '.photo', function() {
			console.log("1111");	
			var id = $(this).data('id');
			getPhoto(this, true, function(result) {
				if(result.status == 1) {
					var pics = result.result.storename;
					var path=getRequestAddressUrl('imageHost')+pics;
					console.log(path);
					baseApi.post({
						op: 'tms',
						method: 'AddAppImage',
						params: {
							"tid": id,
							"url":path,
							"maintid":t.params.ydCode
						}
					}, function(result) {
						if (result.status == 1) {
							mui.toast('上传成功');
							t.listInit();
						} else {
							mui.toast(result.result);
						}
					
					});
				}
			});

		});
		//新建
		$('body').on('click', ".add", function() {
			pageChange("../order/add_driverFees.html", {
				tid: t.params.tid,
				ydCode: t.params.ydCode,
				pageflag: "driver-fees.html"
			});
		});
	}

	// 列表初始化
	pt.listInit = function() {
		var t = this;
		t.element.listContainer.html('');

		// 列表加载
		t.listLoad();
	}

	// 列表加载
	pt.listLoad = function() {
		var t = this;
		t.page.page++;

		if (t.page.more === false) {
			mui('#list-container').pullRefresh().endPulldown();
			mui('#list-container').pullRefresh().endPullup();
			return;
		}

		baseApi.post({
			op: 'tms',
			method: 'GetAllFee',
			params: {
				"tid": t.params.ydCode
			}
		}, function(result) {
			var i = 0;
			var html="";
			if (result.status == 1) {
				result.result.forEach(function(item) {
					i++;
					if(item.ststus=='未审核')
					{
						html = '<li><em  class="fa fa-trash-o del" data-id="' + item.Tid + '"></em>';
						html += '<em class="fa fa-cloud-upload photo" data-id="' + item.Tid + '"></em>';
					    html += '<cite>类型：</cite><em>' + item.Fee_Type + '</em>';
					    html += '<cite style="width:21%" >费用名称：</cite><em style="width:79%">' + item.FeeName + '</em>';
					    html += '<cite>金额：</cite><em>' + item.FeeMoney + '</em>';
					    html += '<cite>状态：</cite><em>' + item.ststus + '</em>';
					    html += '</li>';
					}
					else
					{
						html = '<li>';
						html += '<cite>类型：</cite><em>' + item.Fee_Type + '</em>';
						html += '<cite style="width:21%" >费用名称：</cite><em style="width:79%">' + item.FeeName + '</em>';
						html += '<cite>金额：</cite><em>' + item.FeeMoney + '</em>';
						html += '<cite>状态：</cite><em>' + item.ststus + '</em>';
						html += '</li>';
					}
					
					t.element.listContainer.append(html);
				});
				if (i == 0) {
					mui.toast('暂无记录');
				}
			} else {
				mui.toast('获取失败');
			}
			mui('#list-container').pullRefresh().endPulldown();
		});
	}
	// 页面初始化
	e.page = new p({
		params: {
			tid: getQueryString('tid'),
			ydCode: getQueryString('ydCode'),

		} //获得参数
	});
	mui.init({
		pullRefresh: {
			container: '#list-container',
			down: {
				style: 'circle',
				callback: function() {
					e.page.listInit();
				} // 下拉刷新
			}
		}
	});
	e.page.listInit();
	mui.plusReady(function() {
		mui.init({
			beforeback: function() {
				// 刷新上一页
				plus.webview.currentWebview().opener().evalJS('getWayBillInfo();');
			}
		});
	});

}(window, $, mui);
