! function(e, $, mui) {
	"use strict";

	var config = {};

	// 定义页面
	var p = function(config) {
		var t = this;
		t.config = config;
		t.userInfo = null;
		t.params = config.params;

		t.formdata = {
			feeid: '',
			moneys: '',
			pid: '',
			tid: t.params.ydCode
		};
		t.element = {
			feeid: $('.feeid'),
			moneys: $('.moneys'),
			submit: $('.mui-submit')
		};
		t.init();
	};

	var pt = p.prototype;

	// 配置信息获取
	pt.configReady = function(config_type, callback) {
		var t = this;
		if (config[config_type]) {
			return callback(config[config_type]);
		} else {
			switch (config_type) {
				case 'feeid':
					baseApi.post({
						op: 'tms',
						method: 'FeeItem',
						params: {

						}
					}, function(result) {
						config[config_type] = [];
						if (result.status == 1) {
							result.result.forEach(function(item) {
								config[config_type].push({
									text: item.bof_name,
									value: item.bof_tid
								});
							});
						}

						return callback(config[config_type]);
					});
					break;
			}
		}
	}

	// 页面初始化
	pt.init = function() {
		var t = this;
		t.userInit();
		// 监听
		t.listen();

	};

	// 用户信息初始化
	pt.userInit = function() {
		var t = this;
		t.userInfo = getLocalUserInfo();
	};

	// 事件监听
	pt.listen = function() {
		var t = this;

		// 选择费用项目
		t.element.feeid.parent().on('tap', function() {
			var picker = new mui.PopPicker();
			t.configReady('feeid', function(c) {
				picker.setData(c);
				picker.show(function(item) {
					item = item[0];
					t.element.feeid.text(item.text);
					t.formdata.feeid = item.value;
				});
			});
		});


		// 提交
		t.element.submit.on('tap', function() {
			var formdata = t.formdata;
			formdata.moneys = t.element.moneys.val();
			formdata.pid = t.userInfo.user_id;
			if (!formdata.feeid) {
				mui.toast('请选择费用项目');
				return;
			}
			if (!formdata.moneys) {
				mui.toast('请输入金额');
				return;
			}
			baseApi.post({
					op: 'tms',
					method: 'AddCarFee',
					params: {
						tid: formdata.tid,
						feeid: formdata.feeid,
						moneys: formdata.moneys,
						pid: formdata.pid
					},
				},
				function(result) {
					if (result.status == 1) {
						mui.toast('提交成功');
						// 刷新上一页
						if (isPlusReady()) {
							plus.webview.currentWebview().opener().evalJS('getWayBillInfo();');
							plus.webview.currentWebview().close();
						} else {
							history.go(-1);
						}
					} else {
						mui.toast(result.result);
					}

				});
		});
	}

	// 页面初始化
	e.page = new p({
		params: {
			tid: getQueryString('tid'),
			ydCode: getQueryString('ydCode')
		} //获得参数
	});

	mui.plusReady(function() {
		mui.init({
			beforeback: function() {
				// 刷新上一页
				plus.webview.currentWebview().opener().evalJS('getWayBillInfo();');
			}
		});
	});

}(window, $, mui);
