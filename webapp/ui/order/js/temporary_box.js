!function (e, $, mui) {
    "use strict";
	
	var config = {
	};
	
    var dtPicker = new mui.DtPicker();
    var picker = new mui.PopPicker();
	// 定义页面
	var p = function(config) {
		var t = this;
		t.config = config;
		t.userInfo = null;
		t.params = {
			type: 0	// 0新建 1预约 2失败 3全部
		};
		t.formdata = {
			jg_start_time: '',
			jg_end_time: '',
			sid: '',	// 船公司
			box: '',
			box_no: '',
			ship_name: '',
			voyage: '',
			bill: '',
			vhccode: '',
			tel: ''
		};
		t.element = {
			sid: $('.sid'),
			box: $('.box'),
			box_no: $('.box_no'),
			ship_name: $('.ship_name'),
			voyage: $('.voyage'),
			bill: $('.bill'),
			jg_start_time: $('.jg_start_time'),
			jg_end_time: $('.jg_end_time'),
			submit: $('.mui-submit')
		};
		t.init();
	};
	
	var pt = p.prototype;
	    
    // 配置信息获取
    function configReady(config_type, callback) {
    	if(config[config_type]) {
    		return callback(config[config_type]);
    	} else {
    		switch(config_type) {
    			case 'sid':
    				baseApi.post({
						op: 'company'
					}, function (result) {
						config[config_type] = [];
						result.forEach(function (item) {
							config[config_type].push({
								text: item.sname,
								value: item.sid
							});
						});
						return callback(config[config_type]);
					});
    			break;
    			case 'box':
    				baseApi.post({
						op: config_type
					}, function (result) {
						config[config_type] = [];
						result.forEach(function (item) {
							config[config_type].push({
								text: item.d_value,
								value: item.d_key
							});
						});
						return callback(config[config_type]);
					});
    			break;
    		}
    	}
    }
	
	// 页面初始化
	pt.init = function () {
		var t = this;
		t.userInit();
		// 监听
		t.listen();
	};
	
	// 用户信息初始化
	pt.userInit = function() {
		var t = this;
		t.userInfo = getLocalUserInfo();
		
		t.formdata.vhccode = t.userInfo.vhcCode;
		t.formdata.tel = t.userInfo.phone_tel;
		
		if(!t.userInfo.vhcCode) {
			mui.toast('请先上车后提交');
		}
	};
	
	// 事件监听
	pt.listen = function () {
		var t = this;
		// 选择船公司
		t.element.sid.parent().on('tap', function () {
			configReady('sid', function (c) {
				picker.setData(c);
			    picker.show(function (item) {
			    	item = item[0];
			    	t.element.sid.text(item.text);
			    	t.formdata.sid = item.value;
			    });
			});
		});
		// 选择箱型
		t.element.box.parent().on('tap', function () {
			configReady('box', function (c) {
				picker.setData(c);
			    picker.show(function (item) {
			    	item = item[0];
			    	t.element.box.text(item.text);
			    	t.formdata.box = item.text;
			    });
			});
		});
		// 选择提货时间
		t.element.jg_start_time.parent().on('tap', function () {
		    dtPicker.show(function (item) {
			    t.element.jg_start_time.text(item.value);
		    	t.formdata.jg_start_time = item.value;
		    });
		});
		// 选择提货时间
		t.element.jg_end_time.parent().on('tap', function () {
		    dtPicker.show(function (item) {
			    t.element.jg_end_time.text(item.value);
		    	t.formdata.jg_end_time = item.value;
		    });
		});
		
		// 提交
		t.element.submit.on('tap', function () {
			var formdata = t.formdata;
			formdata.box_no = t.element.box_no.val().trim();
			formdata.ship_name = t.element.ship_name.val().trim();
			formdata.voyage = t.element.voyage.val().trim();
			formdata.bill = t.element.bill.val().trim();
			if(!formdata.sid) {
				mui.toast('请选择船公司');
				return;
			}
			if(!formdata.box) {
				mui.toast('请选择箱型');
				return;
			}
			if(!formdata.box_no) {
				mui.toast('请输入箱号');
				return;
			}
			if(!formdata.ship_name) {
				mui.toast('请输入船名');
				return;
			}
			if(!formdata.voyage) {
				mui.toast('请输入航次');
				return;
			}
			if(!formdata.bill) {
				mui.toast('请输入提单号');
				return;
			}
			if(!formdata.jg_start_time) {
				mui.toast('请选择预计集港时间');
				return;
			}
			if(!formdata.jg_end_time) {
				mui.toast('请选择集港截止时间');
				return;
			}
			
			mui.toast('提交成功');
			if(isPlusReady()) {
				// 刷新上一页
				plus.webview.currentWebview().opener().evalJS('getUserInfo();');
				// 关闭当前页
				plus.webview.currentWebview().close();
			} else {
				history.go(-1);
			}
		});
	}
	
	
	// 页面初始化
	e.page = new p(config);
	mui.plusReady(function () {
		mui.init({
			beforeback: function () {
				// 刷新上一页
				plus.webview.currentWebview().opener().evalJS('getUserInfo();');
			}
		});
	});

}(window, $, mui);