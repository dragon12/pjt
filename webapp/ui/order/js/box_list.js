
!function (e, $, mui) {
    "use strict";
	
	var config = {
	};
	
	// 定义页面
	var p = function(config) {
		var t = this;
		t.config = config;
		t.userInfo = null;
		t.params = config.params;
		
		t.element = {
			listContainer: $('.mui-content')
		};
		t.init();
	};
	
	var pt = p.prototype;
	    
	// 页面初始化
	pt.init = function () {
		var t = this;
		t.userInit();
		// 监听
		t.listen();
		
		// 列表加载
		t.listLoad();
	};
	
	// 用户信息初始化
	pt.userInit = function() {
		var t = this;
		t.userInfo = getLocalUserInfo();
	};
	
	// 事件监听
	pt.listen = function () {
		$('body').on('tap', '.del-box', function() {
			var thisObj = $(this);
			var wb_id = $(this).data('id');
			baseApi.post({
				op: 'delbox',
				wb_id: wb_id
			}, function (result) {
				thisObj.parents('.mui-row').remove();
				mui.toast('删除成功');
			});
		});
	}
	
	// 列表加载
	pt.listLoad = function () {
		var t =this;
		baseApi.post({
			op: 'getbox',
			wmtid: t.params.wmtid
		}, function (result) {
			result.forEach(function (item) {
				var html = '<div class="mui-row">';
				html += '<div class="info mui-pull-left" href="../order/add_box_no.html?wb_id='+item.wb_id+'&wmtid='+t.params.wmtid+'">箱号：<span>'+item.boxno+'</span><br>';
				html += '封号：<span>'+item.sealno+'</span><br>';
				html += '箱型：<span>'+item.box+'</span>';
				html += '</div><div class="action mui-pull-right del-box" data-id="'+item.wb_id+'">删除</div></div>';
				t.element.listContainer.append(html);
			});
		});
	}

	// 页面初始化
	e.page = new p({
		params: {wmtid: getQueryString('wmtid')} //获得参数
	});
	mui.plusReady(function () {
		mui.init({
			beforeback: function () {
				// 刷新上一页
				plus.webview.currentWebview().opener().evalJS('getWayBillInfo();');
			}
		});
	});

}(window, $, mui);

