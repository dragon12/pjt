var itemID = "";
var itemStr = "";
var item;
var maxLength = 3;
var h_finished = "2";
var type = "3";
(function(e) {

		itemID = getQueryString("itemID");
		itemStr = getQueryString("itemStr");
		item = JSON.parse(itemStr);
		if(!isEmpty(item)) {
			$("#h_required_service_date").html(item.h_required_service_date);
			$("#h_linkman").html(item.h_linkman + " " + item.h_linkman_tel);
			$("#h_address").html(item.h_address);
			$("#callno").attr("href", "tel:" + item.h_linkman_tel);
			h_finished = item.h_finished;
			if(h_finished == "1") {

				type = item.h_collect_info[0].h_collect_type;
				if(type == "3") {
					$("#textInput").hide();
					$("#imageInput").show();
				} else {
					$("#imageInput").hide();
					$("#textInput").show();
				}
			} else {
				$("#imageInput").hide();
				$("#textInput").hide();
				$("#phoneIcon").attr("src", "../../../ui/index/images/dianhuabunengan.png");
				$("#commitIcon").attr("src", "../../../ui/index/images/querenbenjincheng_selected.png");
				$("#operating").attr("class", "operating-div none");
			}
		}

})(mui);

var uploadImages = [];
$("#takePhoto").click(function() {
	var imageDiv = document.getElementById("imageDiv");

	var images = imageDiv.getElementsByTagName("img");
	var imageSize = images.length;

	if(this.src.indexOf("add_photo") > -1) {
		//		openBigImage(this.src, true);
		var createImage;
		if(imageSize >= maxLength) {
			createImage = this;
		} else {
			createImage = document.createElement("img");
			createImage.className = "insert-img";
			createImage.style.width = "80px";
			createImage.src = '../../../ui/index/images/add_photo.png';
			insertBefore(createImage, document.getElementById("takePhoto"));
		}
		getPhoto(createImage, false, function(result, file) {
			uploadImages.push(file);
		});
	}
});

$("#commitForture").click(function() {
	if(h_finished == "2") {
		mui.toast("运程已完成");
		return;
	} else if(h_finished == "0") {
		mui.toast("运程未开始");
		return;
	}
	// 百度地图API功能
	var map = new BMap.Map("allmap");
	var point = new BMap.Point(120.389081, 36.073445);
	map.centerAndZoom(point,15);
	map.enableScrollWheelZoom(); //启用地图滚轮放大缩小

	var geolocation = new BMap.Geolocation();
	geolocation.getCurrentPosition(function(r){

		if(this.getStatus() == BMAP_STATUS_SUCCESS){
			console.debug(JSON.stringify(point));
			var parames = {
				"op": "haulsumbit",
				"lng": r.point.lng,
				"lat": r.point.lat,
				"tid": itemID
			}

			if(type == "3") {
				var imageDiv = document.getElementById("imageDiv");
				var images = imageDiv.getElementsByTagName("img");

				if(images.length == 1 && images[0].src.indexOf("add_photo") > -1) {
					mui.toast("请添加照片后提交");
					return;
				}

				exSubmit(parames, uploadImages, function(result) {
					if(result.status == 1) {
						var row = result.result;
						mui.toast("运程提交成功");
						pageChange('../index/page_index.html');
					}
				})
			} else {
				var attrStr = $("#attr").val();
				parames.val = attrStr;
				haulsubmit(parames);
			}
		}
		else {
			alert('failed'+this.getStatus());
		}
	},{enableHighAccuracy: true});

});

function haulsubmit(parames) {
	baseApi.post(parames, function(result) {
		if(result.h_tid) {
			mui.toast("运程提交成功");
			pageChange('../index/page_index.html');
		}
	})
}

function beforeback() {
	if(isPlusReady()) plus.webview.currentWebview().opener().evalJS('getWayBillInfo();');
}