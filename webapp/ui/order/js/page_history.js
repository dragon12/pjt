!function (e, $, mui) {
	"use strict";


	let accept = 2;
	var config = {
	};
	// 定义页面
	var p = function(config) {
		var t = this;
		t.config = config;
		t.userInfo = null;
		t.params = {
			type: 0	// 0新建 1预约 2失败 3全部
		};
		t.page = {
			page: 0,
			perpage: 5,
			more: true
		};
		t.element = {
			nav: $('.top-nav > a'),
			more: $('.list-more'),
			listContainer: $('.mui-content .list')
		};
		t.init();
	};

	var pt = p.prototype;

	// 页面初始化
	pt.init = function () {
		var t = this;
		t.userInit();
		// 监听
		t.listen();
		// 列表初始化
//		t.listInit();
	};

	// 用户信息初始化
	pt.userInit = function() {
		var t = this;
		t.userInfo = getLocalUserInfo();
	};

	// 事件监听
	pt.listen = function () {
		var t = this;
		t.element.nav.on('tap', function () {
			t.params.type = $(this).data('state');
			t.listInit();
		});

		$("#titleTab span").click(function() {
			if(this.className == "active") {
				return;
			}
			$("#titleTab span").attr("class", "");
			$(this).attr("class", "active");
			t.listInit();
		});

		$('body').on('tap', "#waybillList li", function() {
			var id = $(this).data('id');
			pageChange("../order/page_order_info.html", {
				orderID: id,
				showType: 2,
				pageflag: "page_index.html"
			});
		});
		
		$('body').on('tap', "#list-container li", function() {
			var id = $(this).data('id');
			var yd= $(this).children("h4").html();
			var ydCode=yd.toString().trim().split('：');
			pageChange("../order/driver-fees.html", {
				tid: id,
				ydCode:ydCode[1].toString().trim(),
				pageflag: "page_index.html"
			});
		});
	}

	// 列表初始化
	pt.listInit = function () {
		var t = this;
		t.element.listContainer.html('');
		t.page = {
			page: 0,
			perpage: 5,
			more: true
		};

		setTimeout(function () {
			var pullRefresh = mui('#list-container').pullRefresh();
			if(pullRefresh.refresh) pullRefresh.refresh(true);
		}, 1000);
		if(t.params.type == 1) {
			t.element.listContainer.append('<li class="top-notice">预约有效时间为6小时，到期后请重新预约</li>');
		}

		// 列表加载
		t.listLoad();
	}

	// 列表加载
	pt.listLoad = function () {
		var t = this;
		t.page.page++;

		var pullRefresh = mui('#list-container').pullRefresh();

		if(t.page.more === false) {
			if(pullRefresh.endPulldown) pullRefresh.endPulldown();
			if(pullRefresh.endPullup) pullRefresh.endPullup();
			return;
		}

		baseApi.post({
			op: "waybilllist",
			accept: accept,
			row: (t.page.page - 1) * t.page.perpage,
			size: t.page.perpage
		}, function (result) {
			pullRefresh = mui('#list-container').pullRefresh();
			if(pullRefresh.endPullupToRefresh) pullRefresh.endPullupToRefresh();
			pullRefresh = mui('#list-container').pullRefresh();
			if(pullRefresh.endPulldownToRefresh) pullRefresh.endPulldownToRefresh();

			var i = 0;
			result.forEach(function (row) {
				i++;
				var html ='<li data-id="'+row.wm_tid+'"><h4>运单号：' + row.wm_code + '</h4><cite>位置</cite><em>' + row.wm_loading_place + '</em>' +
					'<cite>要求时间</cite><em>' + row.wm_required_service_date + '</em><cite>船公司</cite><em>' + row.ShipCompany + '</em>' +
					'<cite>提单号</cite><em>' + row.wm_awb_code + '</em><cite>箱型箱量</cite><em>' + row.CX + '</em>' +
					'<cite>集装箱号</cite><em>' + row.box_no + '</em><cite>运费</cite><em class="red">' + row.AMTN + '元</em>' +
					'<cite>备注</cite><em>' + row.wm_note + '</em><img class="position-bottom" src="../../../ui/index/images/jinkoutu.png" /></li>';
				t.element.listContainer.append(html);
			});

			if(t.page.page == 1 && result.length == 0) {
				t.element.listContainer.html('<div id="emptyDiv" class="null-list-div" style="height: 617px;"><img src="../../../ui/index/images/no_bill.png"><br><span>暂无运单信息</span></div>');
			}


			pullRefresh = mui('#list-container').pullRefresh();
			if(i < t.page.perpage) {
				t.page.more = false;
				if(pullRefresh.disablePullupToRefresh) pullRefresh.disablePullupToRefresh();
			} else {
				if(pullRefresh.enablePullupToRefresh) pullRefresh.enablePullupToRefresh();
			}
		});
	}

	// 页面初始化
	e.page = new p(config);
	mui.init({
		pullRefresh: {
			container: '#list-container',
			down: {
				style: 'circle',
				contentrefresh: '正在加载...',
				contentdown: '数据加载',
				callback: function () {
					e.page.listInit();
				}	// 下拉刷新
			},
			up: {
				auto: false,
				contentrefresh: '正在加载...',
				contentdown: '数据加载',
				callback: function () {	// 上拉加载
					e.page.listLoad(this);
				}
			}
		}
	});
	e.page.listInit();
	mui.plusReady(function () {
		mui.init({
			beforeback: function () {
				// 刷新上一页
				plus.webview.currentWebview().opener().evalJS('getUserInfo();');
			}
		});
	});

}(window, $, mui);
