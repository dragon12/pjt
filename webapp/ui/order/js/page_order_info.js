var orderID = "";
var isUpdate = "0";
var showType = "2"; //分类型显示 1 待接 ； 2 历史运单； 3 待发;
(function(e) {

	orderID = getQueryString("orderID");
	showType = getQueryString("showType");
	if(isEmpty(showType)) {
		showType = "2";
	}
	
	if(showType == "2") {
		$("#tabTemplate").hide();
	} else if(showType == "1") {
		$("#billType").html("开始运输");
	} else if(showType == "3") {
		$("#billType").html("接单")
	} else {
		$("#tabTemplate").hide();
	}

	if(!isEmpty(orderID)) {
		getWayBillInfo(orderID);
	} else {
		mui.toast("获取详情失败");
	}

})(mui);

function beforeback() {
	if(!isPlusReady()) return;
	var parentView = plus.webview.currentWebview().opener();
	if(parentView.id.indexOf("bill") > -1) {
		parentView.evalJS("pulldownRefresh()");
	}
}

$("#hiddenCon").bind("tap", function() {
	hiddenEle(contactsDrop);
});
var contactsDrop = document.getElementById("contactsDrop");
$("#contacts").bind("tap", function(e) {
	showEle(contactsDrop);
});

$("#cancleBill").bind("tap", function() {
	waybillsubmit(orderID, "0");
});

$("#changeType").bind("tap", function() {
	if($("#billType").html() == "开始运输") {
		waybillsubmit(orderID, "2");
	} else {
		waybillsubmit(orderID, "1");
	}

});

function getWayBillInfo(id) {

	baseApi.post({
		op: "waybill",
		receive: isUpdate,
		wmtid: id
	}, function(result) {
		initDate(result);
	});
}

var waybillInfo = null;
$('.info .qrcode').on('tap', function () {
	if(waybillInfo.ImageUrl) {
	console.log('qrcode clicked');
		openBigImage(waybillInfo.ImageUrl, true);
	}
});

function initDate(result) {
	waybillInfo = result;
	orderID = result.wm_tid;
	if(waybillInfo.ImageUrl) {
		$('.info .qrcode').show();
	} else {
		$('.info .qrcode').hide();
	}
	$("#waybillNo").html(result.wm_code);
	$("#bulidTitle").html(result.wm_type);
	$("#wm_carrier").html(result.wm_carrier);
	$("#billNo").html(result.wm_awb_code);
	$("#paperless").html(result.wm_paperless);
	$("#receiptCode").html(result.wm_receipt_code);
	$("#reserveCode").html(result.wm_reserve_code);
	$("#shapCompany").html(result.ShipCompany);
	$("#address").html(result.wm_loading_place);
	$("#time").html(result.wm_loading_date);
	$("#coType").html(result.CX);
	$("#shapName").html(result.wm_ship + "/" + result.wm_voyage);
	$("#AMTN").html(result.AMTN + "元");
	$("#wm_note").html("备注：" + result.wm_note);
	var hauls = result.haul;
	var contacts = result.contacts;
	var current_haul_id = result.wm_haul_id;
	removeLi("contactsList");
	if(!isEmpty(contacts)) {
		for(var i = 0; i < contacts.length; i++) {
			createContactsItem(contacts[i]);
		}
	}
	if(!isEmpty(hauls)) {
		removeLi("fortuneList");
		for(var i = 0; i < hauls.length; i++) {
			createFortuneItem(hauls[i]);
		}
	}
}

function createFortuneItem(row) {
	var createLi = document.createElement("li");
	createLi.id = row.h_tid;
	createLi.className = row.h_wmtid;
	var typeStr = "";
	if(row.h_required == "1") {
		typeStr = "必须"
	}
	createLi.innerHTML = '<h4>' + row.h_short_name + '</h4><div><span>' + row.h_address + '</span><p>联系人：' + row.h_linkman + ' ' + row.h_linkman_tel + '</p>' +
		'<p>要求时间：' + row.h_required_service_date + ' <em>' + typeStr + '</em></p><cite>备注：' + row.h_note + '</cite></div>';

	$("#fortuneList").append(createLi);
}

function createContactsItem(row) {
	var createLi = document.createElement("li");
	createLi.id = row.tel;
	createLi.className = "goto-type";
	createLi.innerHTML = '<span>' + row.type + '</span><span>' + row.name + '</span><span>' + row.tel + '</span><a href="tel:' + row.tel + '"><img src="../../../ui/index/images/phone_default.png"></a>';
	$("#contactsList").prepend(createLi);
}

function waybillsubmit(id, type) {
	baseApi.post({
		tid: id,
		type: type,
		op: "waybillsubmit"
	}, function(result) {
		if(type == "0") {
			mui.back();
		} else if(type == "1") {
			initDate(result);
			$("#billType").html("开始运输");
		} else if(type == "2") {
			pageChange("../index/page_index.html", {
					referer: '../index/page_index.html',
					isClose: true
				});
		}
	});
}