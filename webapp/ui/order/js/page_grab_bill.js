var currentType = "";

(function(e) {
	$('#content').height($(window).height() - 50).css({'overflow': 'auto'});
	mui.init({
		pullRefresh: {
			container: '#content',
			down: {
				style: 'circle',
				callback: pulldownRefresh
			},
			up: {
				auto: false,
				contentrefresh: '正在加载...',
				callback: pullupRefresh
			}
		}
	});

	wtype();

})(mui);

//下拉刷新
var isRefresh = false;
var accept = 4;

function pulldownRefresh() {
	removeLi("grabBillList");
	isRefresh = true;
	tabPageNum = 0;
	if(mui('#content').pullRefresh()){
		mui('#content').pullRefresh().refresh(true);
	}
	waybilllist(currentType, accept);
}

//上拉加载
var isLoad = false;
function pullupRefresh() {
	isLoad = true;
	waybilllist(currentType, accept)
}

function wtype() {
	baseApi.post({
		op: "wtype"
	}, function(result) {
		for(var i = 0; i < result.length; i++) {
			var str = '<span>' + result[i].item + '</span>';
			if(i == 0) {
				str = '<span class ="active">' + result[i].item + '</span>';
				currentType = result[i].item;
				waybilllist(currentType, 4)
			}
			$("#titleTab").append(str)
		}
		$("#titleTab span").click(function() {
			if(this.className == "active") {
				return;
			}
			$("#titleTab span").attr("class", "");
			$(this).attr("class", "active");
			currentType = this.innerHTML;
			pulldownRefresh();
		});

	});
}

var tabPageNum = 0;
var pageSize = 10;

function waybilllist(type, accept) {
	var pullRefresh = mui('#content').pullRefresh();
	baseApi.post({
		op: "waybilllist",
		type: type,
		accept: accept,
		row: tabPageNum * pageSize,
		size: pageSize
	}, function(result) {

		if(isRefresh) {
			if(pullRefresh.endPulldown) pullRefresh.endPulldown();
			isRefresh = false;
		}
		

		var size = parseInt(result.length);
		if(size > 0) {
			hideEmptyDiv();
			console.log(size + "2");
			tabPageNum += size;
			for(var i = 0; i < size; i++) {
				createItem(result[i]);
			}
		} else if(tabPageNum != 0 && size == 0) {
			isLoad = false;
			mui('#content').pullRefresh().endPullup(true);
		} else {
			var heaght = $("body").height() - $("#header").height();
			emptyShow("暂无抢单信息", heaght, "header");
		}

		$("#grabBillList button").click(function() {
			var id = this.parentElement.id;
			waybillsubmit(id, 1)
		});
		if(isLoad) {
			isLoad = false;
			if(pullRefresh.endPullup) pullRefresh.endPullup();
		}

	}, function() {
		if(isRefresh) {
			if(pullRefresh.endPulldown) pullRefresh.endPulldown();
			isRefresh = false;
		}
		if(isLoad) {
			isLoad = false;
			if(pullRefresh.endPullup) pullRefresh.endPullup();
		}
	});
}

function createItem(row) {
	var createLi = document.createElement("li");
	createLi.id = row.wm_tid;
	createLi.innerHTML = '<div class="station-div"><div><img src="../../../ui/index/images/tihuo.png" /><span>提货地</span>' +
		'<p>' + row.wm_loading_place + '</p></div><div><img class="arrow-right" src="../../../ui/index/images/jiantou.png" /></div><div>' +
		'<img src="../../../ui/index/images/mudi.png" /><span>目的地</span><p>' + row.wm_unloading_place + '</p></div></div>' +
		'<cite>船公司</cite><em>' + row.ShipCompany + '</em><cite>箱型箱量</cite><em>' + row.CX + '</em><cite>备注</cite><em>' + row.wm_note + '</em>' +
		'<cite>运费</cite><em class="red">' + row.AMTN + '元</em><button class="mui-button-row mui-btn-green">抢单</button>';
	$("#grabBillList").append(createLi);
}

function waybillsubmit(id, type) {
	console.log(id)
	baseApi.post({
		tid: id,
		type: type,
		op: "waybillsubmit"
	}, function(result) {
		console.log(result.wm_tid);
		userInfo.current_waybill = result.wm_tid + "";
		localStorage.setItem("user_info", JSON.stringify(userInfo));
		mui.alert('接单后12小时未换单，运单将会自动取消', '接单提醒', '确定', function () {
			
			pageChange("../order/page_order_info.html", {
				orderID: id
			});
		});

	});
}