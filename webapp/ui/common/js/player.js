var video = document.getElementById("video1");
var isPlay = document.querySelector(".switch");
var progress = document.querySelector(".progress");
var loaded = document.querySelector(".progress > .loaded");
var currPlayTime = document.querySelector(".timer > .current");
var totalTime = document.querySelector(".timer > .total");
var playImg = document.getElementById("playImg");
var playerBtn = document.getElementById("playerBtn");
var loadImg = document.getElementById("loadImg");

//(function(){
//    var video, output;
//    var scale = 0.8;
//    var initialize = function() {
//        output = document.getElementById("output");
//        video = document.getElementById("videoElement");
//        video.addEventListener('loadeddata',captureImage);
//    };
//
//    var captureImage = function() {
//        var canvas = document.createElement("canvas");
//        canvas.width = video.videoWidth * scale;
//        canvas.height = video.videoHeight * scale;
//        canvas.getContext('2d').drawImage(video, 0, 0, canvas.width, canvas.height);
//
//        var img = document.createElement("img");
//        img.crossOrigin = "anonymous";
//        img.src = canvas.toDataURL("image/png");
//        output.appendChild(img);
//    };
//
//    initialize();
//})();



//当视频可播放的时候
video.oncanplay = function() {
	//显示视频
	this.style.display = "block";
	loadImg.style.backgroundColor = "#000000";
	showEle(playImg);
	//显示视频总时长
	totalTime.innerHTML = getFormatTime(this.duration); 
};

//播放按钮控制
isPlay.onclick = playClick;

function playClick() {
	hiddenEle(loadImg);
	isPlay.style.display = "block";
	if(video.paused) {
		video.play();
		playerBtn.classList.add("mui-hidden");
		playImg.style.backgroundColor = "rgba(0, 0, 0, 0)";
	} else {
		video.pause();
		playerBtn.classList.remove("mui-hidden");
		playImg.style.backgroundColor = "rgba(0, 0, 0, 0)";
	}
	isPlay.classList.toggle("fa-pause");
};
playImg.addEventListener('tap' , function(){
    playClick();
} , false);
////全屏
//expand.onclick = function() {
//	if(isPlay.classList.contains("fa-pause")) {
//		playClick();
//	}
//	//
//	var Intent = plus.android.importClass("android.content.Intent");
//	var Uri = plus.android.importClass("android.net.Uri");
//	var main = plus.android.runtimeMainActivity();
//	var intent = new Intent(Intent.ACTION_VIEW);
//	var uri = Uri.parse(zylj);
//	intent.setDataAndType(uri, "video/*");
//	main.startActivity(intent);
//};

//播放进度
video.ontimeupdate = function() {
	var currTime = this.currentTime, //当前播放时间
		duration = this.duration; // 视频总时长
	//百分比
	var pre = currTime / duration * 100 + "%";
	//显示进度条
	loaded.style.width = pre;

	//显示当前播放进度时间
	currPlayTime.innerHTML = getFormatTime(currTime);
};

//跳跃播放
progress.onclick = function(e) {
	var event = e || window.event;
	video.currentTime = (event.offsetX / this.offsetWidth) * video.duration;
};

//播放完毕还原设置
video.onended = function() {
	//切换播放按钮状态
	isPlay.classList.remove("fa-pause");
	isPlay.classList.add("fa-play");
	//进度条为0
	loaded.style.width = 0;
	//还原当前播放时间
	currPlayTime.innerHTML = getFormatTime();
	//视频恢复到播放开始状态
	video.currentTime = 0;
};

function getFormatTime(time) {
	var time = time || 0;

	var h = parseInt(time / 3600),
		m = parseInt(time % 3600 / 60),
		s = parseInt(time % 60);
	h = h < 10 ? "0" + h : h;
	m = m < 10 ? "0" + m : m;
	s = s < 10 ? "0" + s : s;

	return m + ":" + s;
}