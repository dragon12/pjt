var IS_OPEN_LOG = false ; 


/**
 *  * 返回生成的随机数字符串
 * @param {Object} start 开始数字 不可为null 默认1
 * @param {Object} end 结束数字 不可为null 默认100
 * @param {Object} count 随机数数量 默认10
 * @param {Object} isInterval 是否分段取值 默认否
 */
function getRandomNumStr(start, end, count, isInterval) {

	setDefaultValue(start, 1);
	setDefaultValue(end, 100);
	setDefaultValue(count, 10);
	setDefaultValue(isInterval, false);

	var countNum = parseInt(count);
	var startNum = parseInt(start);
	var endNum = parseInt(end);
	var intervalNum = (endNum - startNum) / count + 1;
	
	var numStr = "";
	
	if(!isInterval) {
		numStr = createRandomStr(startNum , endNum , countNum);
	} else {
		if(intervalNum > countNum) {
			numStr = createRandomStr(startNum , endNum , countNum)
		} else {
			for(var i = 0; i < intervalNum - 1; i++) {
				var randomNum = Math.round(Math.random() * countNum) + startNum + count * i;
				log(randomNum);
				if(randomNum <= endNum) {
					numStr = numStr + randomNum + ",";
				}
			}
			log(numStr);
			numStr = numStr + createRandomStr(startNum , endNum , countNum - intervalNum);
		}
	}

	return numStr.substr(0, numStr.length - 1);;
}

/**
 * 设置默认值
 * @param {Object} ele
 * @param {Object} defaultValue
 */
function setDefaultValue(ele, defaultValue) {
	if(ele == "" || ele == undefined || ele == 0 || ele == null || ele == "undefined") {
		count = defaultValue;
	}
}
/**
 * 创建随机数字符串
 * @param {Object} startNum
 * @param {Object} endNum
 * @param {Object} countNum
 */
function createRandomStr(startNum , endNum , countNum) {
	var numStr = "";
	for(var i = 0; i < countNum; i++) {
		var randomNum = Math.round(Math.random() * (endNum - startNum)) + startNum;
		if(numStr.indexOf(randomNum + "") > -1) {
			i--;
		} else {
			numStr = numStr + randomNum + ",";
		}
	}
	return numStr ; 
}
/**
 * 控制台日志输出控制
 * @param {Object} info
 */
function log(info){
	if(IS_OPEN_LOG){
		console.log(info);	
	}
}
