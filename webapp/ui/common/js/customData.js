var emptyDiv = document.getElementById("emptyDiv");

function emptyShow(tips, height, id) {
	emptyDiv = document.getElementById("emptyDiv");
	if(isEmpty(emptyDiv)) {
		var createDiv = document.createElement("div");
		createDiv.id = "emptyDiv";
		createDiv.className = "null-list-div";
		createDiv.style.height = height + 'px';
		createDiv.innerHTML = '<img src="../../../ui/index/images/no_bill.png" /><br /><span>' + tips + '</span>';
		document.body.appendChild(createDiv);
		insertAfter(createDiv, document.getElementById(id));
	} else {
		showEle(emptyDiv);
	}
}

function hideEmptyDiv() {
	emptyDiv = document.getElementById("emptyDiv");
	if(!isEmpty(emptyDiv)) {
		hiddenEle(emptyDiv);
	}
}

function openBigImage(src, isHeader) {
	var bigImage = document.getElementById("bigImg");

	if(!isEmpty(bigImage)) {
		console.log('show');
		bigImage.src = src;
		showEle(bigImage);
		return;
	}

	if(isHeader == null || isHeader == undefined) {
		isHeader = false;
	}
	var imageView = document.createElement("img");
	imageView.id = "bigImg";
	imageView.className = "big-img";
	var viewHeagth = document.body.clientHeight;
	if(isHeader) {
		viewHeagth = viewHeagth - $("#header").height() + "px";
	}
	imageView.style.height = viewHeagth;
	imageView.style.top = $("#header").height() + "px";
	imageView.src = src;
	$(".mui-content").append(imageView);

	$("#bigImg").on('tap', function() {
		hideBigImage();
	});

}

function hideBigImage() {
	var bigImage = document.getElementById("bigImg");
	if(!isEmpty(bigImage)) {
		hiddenEle(bigImage);
	}
}

function getPhoto(imageView, isUpload, callBack) {
	imageUpload(imageView, isUpload, callBack);
	return;

	if(isUpload == null || isUpload == undefined) {
		isUpload = true;
	}

	var btnArray = [{
		title: "拍照"
	}, {
		title: "从相册中选择"
	}];
	var text = "";


	plus.nativeUI.actionSheet({
		title: "选择图片",
		cancel: "取消",
		buttons: btnArray
	}, function(e) {
		var index = e.index;
		switch(index) {
			case 0:
				text = "取消选择图片";
				break;
			case 1:
				getImage(imageView, isUpload, callBack);
				break;
			case 2:
				galleryImg(imageView, isUpload, callBack);
				break;
		}
	});
}

function getImage(imageView, isUpload, callBack) {
	imageUpload(imageView, isUpload, callBack);
	return;
	var cmr = plus.camera.getCamera();
	cmr.captureImage(function(path) {
		//		plus.gallery.save(path, function() {
		//
		//		}, function(e) {
		//			mui.toast("拍摄照片失败，请重试");
		//		});

		plus.io.resolveLocalFileSystemURL(path, function(entry) {
			var imgEle = imageView;
			imgEle.src = entry.fullPath;
		}, function() {
			mui.toast("拍摄照片失败，请重试");
		});

		console.log("path==>>" + path);
		plus.zip.compressImage({
				src: path,
				dst: path,
				overwrite: true,
				quality: 20
			},
			function() {
				if(isUpload) {
					createUpload(path, callBack);
				} else {
					callBack(path);
				}

			},
			function(error) {
				alert("图片上传失败，请重新选择！");
			});
	}, function(e) {
		mui.toast('取消拍照');
	}, {
		filename: '_doc/gallery/',
		index: 1
	});
}

function galleryImg(imageView, isUpload, callBack) {
	// 从相册中选择图片
	plus.gallery.pick(function(path) {

		var imgEle = imageView;
		imgEle.src = path;
		plus.zip.compressImage({
				src: path,
				dst: path,
				overwrite: true,
				quality: 20
			},
			function() {

				if(isUpload) {
					createUpload(path, callBack);
				} else {
					callBack(path);
				}
			},
			function(error) {
				alert("图片上传失败，请重新选择！");
			});

	}, function(e) {
		mui.toast('取消选择图片');
	}, {
		filter: 'image'
	});
}

function createUpload(path, callback) {
	console.log(path);
	plus.nativeUI.showWaiting();
	var task = plus.uploader.createUpload(getRequestAddressUrl('base'), {
		method: "POST",
		priority: 100
	}, function(t, status) {
		plus.nativeUI.closeWaiting();
		// 上传完成
		if(status == 200) {
			console.log(JSON.stringify(t));
			var result = JSON.parse(t.responseText);
			callback(result);
		} else {
			mui.toast("上传失败!");
		}
	});
	var length = path.split("/").length - 1;
	var name = path.split("/")[length];
	task.addFile(path, {
		key: "f",
		name: name,
		mime: "image/jpeg"
	});
	
	var jsonDate = {
		"op": "uploadfile",
		"uid": userInfo.user_id,
		system: systemName
	}
	task.addData("p", JSON.stringify(jsonDate));
	task.addData("alert", "1");
	task.start();
}

function exSubmit(parames, images, callback) {
	try {
		var r = new XMLHttpRequest;
		r.open("post", getRequestAddressUrl('base'), true);
		r.setRequestHeader("X-Requested-With", "XMLHttpRequest");
		r.onreadystatechange = function() {
			if (r.readyState == 4) {
				var result = JSON.parse(r.responseText);
				callback(result);
			}
		};
		var num = images.length;
		if(userInfo) {
			parames.uid = userInfo.user_id;
			parames.system = systemName;
			parames.num = num;
		}
		let postData = new FormData;
		postData.append("p", JSON.stringify(parames));
		postData.append("alert", "1");
		// 添加上传文件
		for(let i = 0; i < images.length; i++) {
			let file = images[i];
			let positionNo = i + 1;
			postData.append("image" + positionNo, file);
		}
		result = r.send(postData)
	} catch(o) {
		console.log(o);
		alert(o)
	}
}